<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'prefix' => 'v1',
    'middleware' => ['api']
], function () {

    Route::post("/createCaptcha", "Api\CaptchaController@generateCaptcha");
    Route::post("/verifyCaptcha", "Api\CaptchaController@verifyCaptcha");
    //获取认证用户信息
    Route::post("/userInfo", "Api\UserController@info");
    Route::get("/userList", "Api\UserController@list");//显示代理商系统用户

    //代理商相关
    Route::post("/agentCreate", "Api\AgentController@createAgent");//代理商 创建
    Route::post("/agentSelectorList", "Api\AgentController@selectorOptionsList");//代理商下拉 搜索
    Route::get("/agentList", "Api\AgentController@list"); //代理商列表
    Route::post("/agentInfo", "Api\AgentController@info");//代理商详细信息
    Route::post("/agentChildrenList", "Api\AgentController@childrenAgent");//获取代理商的后代代理商

    Route::post("/agentChangeAdminPassword", "Api\AgentController@changeAgentAdminUserPassword");//修改代理商管理员密码
    Route::post("/agentChangeBasic", "Api\AgentController@updateAgentBasic");//修改代理商基本信息
    Route::post("/agentChangeInfo", "Api\AgentController@updateAgentInfo");//修改代理附加信息
    Route::post("/agentChangePercentage", "Api\AgentController@updateAgentPercentage");//修改代理商分成比例配置
    Route::post("/agentCapitalPoolListWithInfo", "Api\AgentController@agentCapitalPoolListWithInfo");//代理商资金池和默认默认资金池


    //短信
    Route::get("/agentMessageList", "Api\UMsgController@list");//短信列表



    //代理商提现
    Route::get('/agentCashList', 'Api\AgentCashController@list');
    Route::post('/agentCashInfo', 'Api\AgentCashController@info');
    Route::post('/agentCashUpdate', 'Api\AgentCashController@update');

    //代理商提现申请
    Route::post('/agentCashOutInfo', 'Api\AgentCashOutController@info');
    Route::post('/agentCashOutUpdate', 'Api\AgentCashOutController@addCashOutRecord');




    //代理商修改自己的提现银行卡
    Route::post('/agentBankCardSendSms', 'Api\AgentWithdrawBankcardCardController@sendMsmCode');
    Route::post('/agentBankCardInfo', 'Api\AgentWithdrawBankcardCardController@info');
    Route::post('/agentBankCardUpdate', 'Api\AgentWithdrawBankcardCardController@update');



    Route::post('/recommendCode', 'Api\RecommentCodeController@info');//获取代理商或者员工的推荐码


    //代理商员工列表 分页 搜索员工
    Route::get("/employeeList", "Api\EmployeeController@list");
    Route::post("/employeeCreate", "Api\EmployeeController@create");
    Route::post("/employeeInfo", "Api\EmployeeController@info");
    Route::post("/employeeUpdate", "Api\EmployeeController@update");


    //客户相关
    Route::get("/clientList", "Api\ClientController@list");//客户列表
    Route::post("/clientInfo", "Api\ClientController@info");//客户详情
    Route::post("/clientUpdate", "Api\ClientController@update");//客户更新
    Route::post("/getClientInfoWithHeritSelectorList", "Api\ClientController@clientInfoWithHeritSelectorList");//跟换客户关系,下拉刷新
    Route::post("/swapClientHeritRelation", "Api\ClientController@changeClientAgentEmployeeRelations");//更改客户关系
    Route::post("/issueClientToken", "Api\ClientController@issueClientToken");//产生前台登陆的token


    //用户账户余额调整
    Route::post("/clientAcountFlowAdjust", "Api\ClientFlowController@clientAcountFlowAdjust");
    Route::get("/clientAccountList", "Api\ClientFlowController@list");


    //银行卡信息
    Route::get("/bankCardList", "Api\ClientBankCardController@list");//列表
    Route::post("/bankCardUpdate", "Api\ClientBankCardController@update");//更新
    Route::post("/bankCardInfo", "Api\ClientBankCardController@info");//获取单挑银行卡信息

    Route::get("/withdrawList", "Api\ClientWithdrawController@list");
    Route::post("/withdrawUpdate", "Api\ClientWithdrawController@update");
    Route::post("/withdrawInfo", "Api\ClientWithdrawController@info");


    //用户账户充值
    Route::get("/clientRechargeList", "Api\ClientRechargeController@list");
    Route::post("/clientAcountRecharge", "Api\ClientRechargeController@clientAcountRecharge");
    Route::post("/clientRechargeInfo", "Api\ClientRechargeController@info");
    Route::post("/clientRechargeUpdate", "Api\ClientRechargeController@update");

    //代理商管理费分成

    Route::get("/agentServiceFeeList", "Api\AgentServiceFeeController@list");




    //系统设置
    Route::post("/systemParams", "Api\System\SystemParamsController@index");
    Route::post("/systemParam/update/{id?}", "Api\System\SystemParamsController@update");
    Route::post("/systemParam/{id?}", "Api\System\SystemParamsController@show");
    //节假日维护
    Route::post("/holidayMaintain/update/{id}", "Api\System\HolidayMaintainController@update");
    Route::post("/holidayMaintain/destroy/{id}", "Api\System\HolidayMaintainController@destroy");
    Route::post("/holidayMaintains", "Api\System\HolidayMaintainController@index");
    Route::post("/holidayMaintain/store", "Api\System\HolidayMaintainController@store");
    Route::post("/holidayMaintain/{id}", "Api\System\HolidayMaintainController@show");
    //股票配资产品
    Route::post("/stockFinanceProduct/update/{id}", "Api\System\StockFinanceProductController@update");
    Route::post("/stockFinanceProduct/destroy/{id}", "Api\System\StockFinanceProductController@destroy");
    Route::post("/stockFinanceProducts", "Api\System\StockFinanceProductController@index");
    Route::post("/stockFinanceProduct/store", "Api\System\StockFinanceProductController@store");
    Route::post("/stockFinanceProduct/{id}", "Api\System\StockFinanceProductController@show");
    //股票信息
    Route::post("/stockInfo/update/{id}", "Api\System\StockInfoController@update");
    Route::post("/stockInfo/destroy/{id}", "Api\System\StockInfoController@destroy");
    Route::post("/stockInfos", "Api\System\StockInfoController@index");
    Route::post("/stockInfo/store", "Api\System\StockInfoController@store");
    Route::post("/stockInfo/{id}", "Api\System\StockInfoController@show");
    //费用标准
    Route::post("/stockFee/update/{id}", "Api\System\StockFeeController@update");
    Route::post("/stockFees", "Api\System\StockFeeController@index");
    Route::post("/stockFee/store", "Api\System\StockFeeController@store");
    Route::post("/stockFee/{id}", "Api\System\StockFeeController@show");
    //风控规则
    Route::post("/transRistControlRule/update/{id}", "Api\System\TransRistControlRuleController@update");
    Route::post("/transRistControlRules", "Api\System\TransRistControlRuleController@index");
    Route::post("/transRistControlRule/store", "Api\System\TransRistControlRuleController@store");
    Route::post("/transRistControlRule/{id}", "Api\System\TransRistControlRuleController@show");


    //代理商递延费
    Route::get("/agent/interestList", "Api\AgentInterestController@list");



    //操盘账户（就是子账户/风控管理/配资记录）
    Route::post("/uStockFinancing/update/{id}", "Api\System\UStockFinancingController@update");
    Route::post("/uStockFinancings", "Api\System\UStockFinancingController@index");
    Route::post("/uStockFinancings/riskList", "Api\System\UStockFinancingController@riskList");
    Route::post("/uStockFinancing/{id}", "Api\System\UStockFinancingController@show");
    //持仓
    Route::post("/uStockFinanceHoldings", "Api\System\UStockFinanceHoldingController@index");

    //配资付息记录
    Route::post("/uStockFinanceInterestPercentages", "Api\System\UStockFinanceInterestPercentageController@index");
    Route::post("/uStockFinanceInterestPercentages/stock_finance_id/{stock_finance_id}",
        "Api\System\UStockFinanceInterestPercentageController@stock_finance_index");
    Route::post("/uStockFinanceInterestPercentage/{id}", "Api\System\UStockFinanceInterestPercentageController@show");
    //业务审核（结算审核 已取消）
    //Route::post("/uStockFinanceFettleup", "Api\System\UStockFinanceFettleupController@index");
    //Route::post("/uStockFinanceFettleup/{id}", "Api\System\UStockFinanceFettleupController@show");
    //除权降息管理
    Route::post("/xrDrInfos", "Api\System\XrDrInfoController@index");
    Route::post("/xrDrInfo/{id}", "Api\System\XrDrInfoController@show");
    //资金池管理
    Route::post("/capitalPool/update/{id}", "Api\System\CapitalPoolController@update");
    Route::post("/capitalPools", "Api\System\CapitalPoolController@index");
    Route::post("/capitalPool/store", "Api\System\CapitalPoolController@store");
    Route::post("/capitalPool/{id}", "Api\System\CapitalPoolController@show");
    //操盘母账户管理
    Route::post("/parentStockFinance/update/{id}", "Api\System\ParentStockFinanceController@update");
    Route::post("/parentStockFinances", "Api\System\ParentStockFinanceController@index");
    Route::post("/parentStockFinance/store", "Api\System\ParentStockFinanceController@store");
    Route::post("/parentStockFinance/{id}", "Api\System\ParentStockFinanceController@show");

    //当日委托
    Route::post("/uStockFinanceEntrusts", "Api\System\UStockFinanceEntrustController@index");
    //当日成交
    Route::post("/uStockFinanceDayMakedeals", "Api\System\UStockFinanceDayMakedealController@index");
    //母账户当日手动买入或卖出(委托表)
    Route::post("/uParentStockFinanceEntrusts", "Api\System\UParentStockFinanceEntrustController@index");
    //母账户历史手动买入或卖出(委托表)
    Route::post("/uParentStockFinanceEntrustHistorys", "Api\System\UParentStockFinanceEntrustHistoryController@index");
    //历史委托
    Route::post("/uStockFinanceEntrustHistorys", "Api\System\UStockFinanceEntrustHistoryController@index");
    //历史成交
    Route::post("/uStockFinanceDayMakedealHistorys", "Api\System\UStockFinanceDayMakedealHistoryController@index");
    //资金流水
    Route::post("/uStockFinancingFlows", "Api\System\UStockFinancingFlowController@index");
    //风控日志管理
    Route::post("/stockFinanceRiskLogs", "Api\System\StockFinanceRiskLogController@index");
    //佣金统计
    Route::post("/uStockFinanceDayMakedeals/fee_list", "Api\System\UStockFinanceDayMakedealController@fee_list");

    //访问javaApi
    Route::post("/javaApi/eveningUp", "Api\System\JavaApiController@eveningUp");
    Route::post("/javaApi/eveningUpPerHolding", "Api\System\JavaApiController@eveningUpPerHolding");
    Route::post("/javaApi/doXrdr", "Api\System\JavaApiController@doXrdr");

    Route::post("/javaApi/assignBuyDayMakeDeal", "Api\System\JavaApiController@assignBuyDayMakeDeal");
    Route::post("/javaApi/assignBuyHistoryMakeDeal", "Api\System\JavaApiController@assignBuyHistoryMakeDeal");
    Route::post("/javaApi/assignSellDayMakeDeal", "Api\System\JavaApiController@assignSellDayMakeDeal");
    Route::post("/javaApi/assignSellHistoryMakeDeal", "Api\System\JavaApiController@assignSellHistoryMakeDeal");

    Route::post("/javaApi/deliveryStock", "Api\System\JavaApiController@deliveryStock");//手动派股票
    Route::post("/javaApi/retrieveStock", "Api\System\JavaApiController@retrieveStock");//股票回收
    Route::post("/javaApi/adjustBalance", "Api\System\JavaApiController@adjustBalance");//修改母账户余额
    Route::post("/javaApi/settleup", "Api\System\JavaApiController@settleup");//结算

    //管理后台用户相关
    Route::post('/logout', "Api\UserController@logoutApi");//注销
    Route::post('/rolePlayIssueToken', "Api\UserController@rolePlayIssueToken");//扮演用户
    Route::get("/userList", "Api\UserController@list");//用户列表


    //代理商报表
    Route::post("/report/agentDayAchievementReports", "Api\System\UAgentDayAchievementReportController@main_index");//日报表主页
    Route::post("/report/agentDayAchievementReportsChildren", "Api\System\UAgentDayAchievementReportController@children_index");//日报表展开
    Route::post("/report/agentMaxPerformanceReports", "Api\System\UAgentMaxPerformanceReportController@main_index");//峰值表
    Route::post("/report/agentMaxPerformanceReportsChildren", "Api\System\UAgentMaxPerformanceReportController@children_index");//峰值表展开

    Route::get("/report/clientDayReports", "Api\Report\ClientDayReportController@list");//客户日报表
    Route::get("/report/clientFullReports", "Api\Report\ClientFullReportController@list");//客户总报表
    Route::get("/report/employeeDayReports", "Api\Report\EmployeeDayReportController@list");//员工日报表
    Route::get("/report/employeeFullReports", "Api\Report\EmployeeFullReportController@list");//员工总报表
    Route::get("/report/employeeTotalReports", "Api\Report\EmployeeDayReportController@totalList");//员工业绩总报表

    //内容管理
    Route::get("/protocolList", "Api\ContentProtocolController@protocols");
    Route::post("/getProtocolInfo", "Api\ContentProtocolController@getProtocolInfo");
    Route::get("/protocolTypeList", "Api\ContentProtocolController@protocolTypes");
    Route::post("/protocolEdit", "Api\ContentProtocolController@protocolEdit");
    Route::post("/protocolDelete", "Api\ContentProtocolController@protocolDelete");
    Route::get("/helpTypeList", "Api\ContentHelpTypeController@helpTypes");
    Route::post("/getHelpTypeInfo", "Api\ContentHelpTypeController@getHelpTypeInfo");
    Route::post("/helpTypeEdit", "Api\ContentHelpTypeController@helpTypeEdit");
    Route::post("/helpTypeDelete", "Api\ContentHelpTypeController@helpTypeDelete");
    Route::get("/helpList", "Api\ContentHelpController@helps");
    Route::post("/getHelpInfo", "Api\ContentHelpController@getHelpInfo");
    Route::post("/helpEdit", "Api\ContentHelpController@helpEdit");
    Route::post("/helpDelete", "Api\ContentHelpController@helpDelete");
    Route::get("/articleList", "Api\ContentArticleController@articles");
    Route::post("/getArticleInfo", "Api\ContentArticleController@getArticleInfo");
    Route::get("/articleTypeList", "Api\ContentArticleController@articleTypes");
    Route::post("/articleEdit", "Api\ContentArticleController@articleEdit");
    Route::post("/articleDelete", "Api\ContentArticleController@articleDelete");
    Route::post("/uploadPic", "Api\OtherController@uploadPic");
});
