@setup

$php_api_dir ='/home/admin.api.qmmian.cn';
$php_admin_api_git_url = 'git@10.10.1.120:php/api.git';
$branch = 'admin-dev';

$admin_web_dir = '/home/admin.web.qmmian.cn';
$admin_web_url = 'git@10.10.1.120:xuryin/backstage-view.git';


$giteeWebUrl = 'git@gitee.com:02hero/vuejs-admin-spa.git';
$giteeApiUrl = 'git@gitee.com:02hero/laravel-admin-api.git';


@endsetup

@servers(['web' => ['root:yingli@10.10.1.111']])

@task('111', ['on' => 'web'])
source ~/.bashrc
source /etc/profile
source ~/.bash_profile
whoami
if [ ! -d "{{$php_api_dir}}" ]; then
#文件不存在
git clone {{$php_admin_api_git_url}} {{$php_api_dir}}
fi

cd {{$php_api_dir}}
git reset --hard
git pull
git checkout {{$branch}}

composer install
php artisan down

#安装composer包
#需要手动配置.env文件
php artisan config:cache
#php artisan view:clear
echo '初始化laravel-passport包'
#php artisan passport:install
echo '给laravel storage 777 权限'
chmod -R 777 {{$php_api_dir}}/storage
php artisan up


echo 'ali php-app'
scp -C -r /home/admin.api.qmmian.cn/app root@39.108.6.55:/home/admin.api.qmmian.cn/app

echo 'ali php-config'
scp -C -r /home/admin.api.qmmian.cn/config root@39.108.6.55:/home/admin.api.qmmian.cn/config

echo 'ali php-routers'
scp -C -r /home/admin.api.qmmian.cn/routes root@39.108.6.55:/home/admin.api.qmmian.cn/routes





echo '部署vuejs管理后台'
if [ ! -d "{{$admin_web_dir}}" ]; then
#文件不存在 克隆代码
cd /home
git clone {{$admin_web_url}} {{$admin_web_dir}}
fi

cd {{$admin_web_dir}}
echo '清洁git'
git reset --hard
echo 'git pull 代码'
git pull
echo '安装npm包'

cnpm install

echo '编译vuejs'
npm run build
echo 'vuejs 部署完成'

echo 'vuej spa'
scp -C -r /home/admin.web.qmmian.cn/dist root@39.108.6.55:/home/admin.web.qmmian.cn/dist

@endtask

@task('ali', ['on' => 'web'])

echo 'ali php-app'
scp -C -r /home/admin.api.qmmian.cn/app root@39.108.6.55:/home/admin.api.qmmian.cn/

echo 'ali php-config'
scp -C -r /home/admin.api.qmmian.cn/config root@39.108.6.55:/home/admin.api.qmmian.cn/

echo 'ali php-routers'
scp -C -r /home/admin.api.qmmian.cn/routes root@39.108.6.55:/home/admin.api.qmmian.cn/

echo 'vuejs-dist'
scp -C -r /home/admin.web.qmmian.cn/dist root@39.108.6.55:/home/admin.web.qmmian.cn/

@endtask
