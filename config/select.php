<?php
return [
    'SystemParams' => [
        'whereMust' => ''
    ],
    'StockFinanceProduct' => [
        'whereMust' => ''
    ],
    'StockFee' => [
        'whereMust' => ''
    ],
    'StockInfo' => [
        'whereMust' => ''
    ],
    'ParentStockFinance' => [
        'whereMust' => ''
    ],
    'UStockFinanceInterestPercentage' => [
        'keyword' => 'agent_n'
    ],
    'UStockFinancing' => [
        'keyword' => 'agent_id'
    ],
    'UStockFinanceEntrust' => [
        'keyword' => 'agent_id'
    ],
    'UStockFinanceEntrustHistory' => [
        'keyword' => 'agent_id'
    ],
    'UStockFinanceDayMakedeal' => [
        'keyword' => 'agent_id'
    ],
    'UStockFinanceDayMakedealHistory' => [
        'keyword' => 'agent_id'
    ],
    'UStockFinanceHolding' => [
        'keyword' => 'agent_id'
    ],
    'UStockFinancingFlow' => [
        'keyword' => ['has' => 'u_stock_financing']
    ],
    'StockFinanceRiskLog' => [
        'keyword' => 'agent_id'
    ],
    'limit_default' => 1000
];
