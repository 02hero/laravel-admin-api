<?php

namespace App;

use App\Http\Model\ClientAgentEmployeeRelation;
use App\Http\Model\Employee;
use App\Http\Model\Role;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    const CREATED_AT = "created_time";
    const UPDATED_AT = "updated_time";
    use Notifiable, HasApiTokens;

    protected $table = "a_agent_emp";
    protected $guarded = ['id', 'create_time', 'updated_time'];
    protected $hidden = ['password'];

    //自定义passport 登陆用户名 id 可以改成其他字段
    public function findForPassport($username)
    {
        return $this->where('phone', $username)->orWhere('name', $username)->first();
    }

//验证密码
    public function validateForPassportPasswordGrant($password)
    {
        return Hash::check($password, $this->password) || $this->password = md5(md5($password));
    }

    public function AauthAcessToken()
    {
        return $this->hasMany('\App\OauthAccessToken');
    }


    public function agent()
    {
        return $this->belongsTo('App\Http\Model\Agent');
    }

    public function role()
    {
        return $this->belongsTo('App\Http\Model\Role');
    }

    public function percentages()
    {
        return $this->hasMany('App\Http\Model\EmployeeProfitRateConfig', 'employee_id');
    }

    //是否是系统管理
    public function getIsSystemAdminAttribute()
    {
        return $this->role_id == Role::ROLE_ADMIN_SYSTEM;
    }

    //是否是系统管理
    public function getIsAgentAdminAttribute()
    {
        return $this->role_id == Role::ROLE_ADMIN_AGENT;
    }

    /**
     * 获取全部代理商的下级客户
     * @return array
     */
    public function getAllMyClientIdArrayAttribute()
    {
        $agent = $this->agent;
        $key = "agent{$agent->agent_level}";
        $vo = $agent->id;
        $clients = ClientAgentEmployeeRelation::where([$key => $vo])->pluck('cust_id')->toArray();
        return array_values($clients);
    }

    /**
     * 获取我的直接客户ids
     * @return array
     */
    public function getMyDirectClientIdArrayAttribute()
    {
        $clients = ClientAgentEmployeeRelation::where('direct_agent_id', $this->agent_id)->pluck('cust_id')->toArray();
        return array_values($clients);
    }

    /**
     * 获取登陆代理商的全部员工(只能拉客户)
     * @return array
     */
    public function getAllMyEmployeeIdArrayAttribute()
    {
        //根据干总董总 只有id=16的才能拉到客户其他的用户不能拉客户
        $empIds = Employee::where(['agent_id' => $this->agent_id, 'role_id' => Role::ROLE_STAFF_AGENT])->pluck('id')->toArray();
        return array_values($empIds);
    }


}
