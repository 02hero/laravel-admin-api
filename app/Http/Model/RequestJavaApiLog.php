<?php

namespace App\Http\Model;

class RequestJavaApiLog extends Base
{
    protected $table = "s_request_java_api_log";

    protected $guarded = ['id', 'created_time'];
}
