<?php

namespace App\Http\Model;


/**
 * App\Http\Model\Role
 */
class Role extends Base
{
    protected $table = "s_system_role";
    protected $guarded = ['id'];
    public $timestamps = false;

    const ROLE_ADMIN_SYSTEM = 1;
    const ROLE_ADMIN_AGENT = 11;
    const ROLE_STAFF_AGENT = 16;//这个角色的员工可以进行推广,可以分成,其他的员工不能分成




}
