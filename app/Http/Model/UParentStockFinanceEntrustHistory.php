<?php

namespace App\Http\Model;

class UParentStockFinanceEntrustHistory extends Base
{
    protected $table = "u_parent_stock_finance_entrust_history";

    protected $guarded = ['id', 'created_time', 'updated_time'];

    /**
     * hasMany成交
     */
    public function makedeal()
    {
        return $this->hasMany(UParentStockFinanceDayMakedealHistory::Class, 'parent_entrust_id', 'parent_finance_entrust_id');
    }

    /*
     * 获取成交ids
     */
    public function getMakedealIdsAttribute()
    {
        return implode(',', $this->relations['makedeal']->pluck('parent_makedeal_id')->all());
    }

    /**
     *  母账户委托状态
     *
     * @param  string $value
     * @return string
     */
    public function getParentEntrustStatusAttribute($value)
    {
        switch ($value) {
            case 1:
                return '未成交';
            case 2:
                return '部分成交';
            case 3:
                return '部成部撤';
            case 4:
                return '已撤单';
            case 5:
                return '已成交';
            case 6:
                return '委托失败';
            default:
                return '未知';
        }
    }

    /**
     *  买卖方向
     *
     * @param  string $value
     * @return string
     */
    public function getSellBuyAttribute($value)
    {
        switch ($value) {
            case 1:
                return '买入';
            case 2:
                return '卖出';
            default:
                return '未知';
        }
    }

    /**
     *  成交总价
     *
     * @param  string $value
     * @return string
     */
    public function getBargainPriceAttribute($value)
    {
        return round($value, 3);
    }
}
