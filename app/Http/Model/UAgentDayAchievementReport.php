<?php

namespace App\Http\Model;

class UAgentDayAchievementReport extends Base
{
    public $timestamps = false;
    protected $table = "u_agent_day_achievement_report";
    protected $guarded = ['id'];

    /**
     * belongsTo代理商
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agent()
    {
        return $this->belongsTo(Agent::class, 'agent_id', 'id');
    }

    public function getDirectCustDayInchargeAttribute($value)
    {
        return round($value, 3);
    }

    public function getDirectCustDayCommisionAttribute($value)
    {
        return round($value, 3);
    }

    public function getDirectCustDayInterestsAttribute($value)
    {
        return round($value, 3);
    }

    public function getDirectCustDayFeeAttribute($value)
    {
        return round($value, 3);
    }

    public function getDirectCustDayMakeDealAttribute($value)
    {
        return round($value, 3);
    }

    public function getDirectCustDayStockFinanceAttribute($value)
    {
        return round($value, 3);
    }

    public function getDirectCustAdvDayCostAttribute($value)
    {
        return round($value, 3);
    }

    /**
     *  总当日充值
     */
    public function getTotalInchargeAttribute($value)
    {
        return round($value, 3);
    }

    /**
     *  总当日提现
     */
    public function getTotalCashAttribute($value)
    {
        return round($value, 3);
    }

    /**
     *  总当日配资金额
     */
    public function getTotalStockFinanceAttribute($value)
    {
        return round($value, 3);
    }

    /**
     *  总当日交易金额
     */
    public function getTotalMakeDealAttribute($value)
    {
        return round($value, 3);
    }

    /**
     *  总当日手续费(不能提成)/包含6项手续费+佣金
     */
    public function getTotalFeeAttribute($value)
    {
        return round($value, 3);
    }

    /**
     *  总当日利息/递延费
     */
    public function getTotalInterestsAttribute($value)
    {
        return round($value, 3);
    }

    /**
     *  总代理个体当日利息/递延费
     */
    public function getTotalInterestsPercentageAttribute($value)
    {
        return round($value, 3);
    }

    /**
     *  总当日佣金
     */
    public function getTotalCommisionAttribute($value)
    {
        return round($value, 3);
    }

    /**
     *  总代理个体当日佣金
     */
    public function getTotalCommisionPercentageAttribute($value)
    {
        return round($value, 3);
    }

    /**
     *  总当日推广成本
     */
    public function getTotalCostAttribute($value)
    {
        return round($value, 3);
    }


}
