<?php

namespace App\Http\Model;

class UStockFinanceSettleup extends Base
{
    protected $table = "u_stock_finance_settleup";

    protected $guarded = ['id', 'created_time', 'updated_time'];

}
