<?php

namespace App\Http\Model;

use Illuminate\Support\Facades\Redis;

class UStockFinancing extends Base
{
    protected $table = "u_stock_financing";

    protected $guarded = ['id', 'created_time', 'updated_time'];

    /**
     * belongsTo客户
     */
    public function client()
    {
        return $this->belongsTo(Client::Class, 'cust_id', 'id');
    }

    /**
     * belongsTo收费
     */
    public function stock_fee()
    {
        return $this->belongsTo(StockFee::Class, 'relation_id', 'agent_id');
    }

    /**
     * belongsTo产品
     */
    public function stock_finance_product()
    {
        return $this->belongsTo(StockFinanceProduct::Class, 'product_id', 'id');
    }

    /**
     * hasOne结算
     */
    public function u_stock_finance_settleup()
    {
        return $this->hasOne(UStockFinanceSettleup::Class, 'stock_finance_id', 'id');
    }

    /**
     * hasMany持仓
     */
    public function u_stock_finance_holding()
    {
        return $this->hasMany(UStockFinanceHolding::Class, 'stock_finance_id', 'id');
    }


    /**
     *  stock_Finance
     *
     * @param  string $value
     * @return string
     */
    public function getStockFinanceAttribute()
    {
        //$StockFinanceInfo = json_decode(Redis::hGet('StockFinanceInfo', $this->attributes['id']), true);废弃 资产自己算。。
        if ($this->attributes['status'] != 4) {
            $rs = UStockFinanceHolding::select('holdings_quantity', 'stock_code')->where('stock_finance_id', $this->attributes['id'])->get();
            $arr['stockFinanceMarketValue'] = 0;
            if ($rs) {
                $price = 0;
                $arr['stockFinanceMarketValue'] = 0;
                foreach ($rs as $v) {
                    $stockInfo = json_decode(Redis::hGet('stockmarket', $v['stock_code']), true);
                    if (isset($stockInfo['price'])) {
                        $price = $stockInfo['price'] ?: $stockInfo['preClose'];
                        $arr['stockFinanceMarketValue'] += $v['holdings_quantity'] * $price;
                    } else {
                        $arr['stockFinanceMarketValue'] = '行情错误';
                        $arr['totalAssert'] = '未知';
                        $arr['netAsserts'] = '未知';
                        $arr['winLoss'] = '未知';
                        $arr['holdingRate'] = '未知';
                        $arr['precautiousGap'] = '未知';
                        $arr['liquidationGap'] = '未知';
                        return $this->attributes['stock_finance'] = $arr;
                    }
                }
                $arr['stockFinanceMarketValue'] = round($arr['stockFinanceMarketValue'], 3);
            }
            $arr['totalAssert'] = round($arr['stockFinanceMarketValue'] + $this->attributes['available_amount']
                + $this->attributes['freeze_buying_money'] + $this->attributes['freeze_charge_money'], 3);
            $arr['netAsserts'] = round($arr['totalAssert'] - $this->attributes['current_finance_amount'], 3);
            $arr['winLoss'] = round($arr['netAsserts'] - $this->attributes['init_caution_money']
                - $this->attributes['post_finance_caution_money'] - $this->attributes['post_add_caution_money'], 3);
            $arr['holdingRate'] = $arr['totalAssert'] > 0 ? round($arr['stockFinanceMarketValue'] / $arr['totalAssert'], 4) : 0;
            $arr['precautiousGap'] = round($arr['totalAssert'] - $this->attributes['precautious_line_amount'], 3);
            $arr['liquidationGap'] = round($arr['totalAssert'] - $this->attributes['liiquidation_line_amount'], 3);
        } else {
            $arr['stockFinanceMarketValue'] = 0;
            $rs = UStockFinanceSettleup::select('stock_finance_id', 'gain_loss', 'gain_loss_amount', 'contract_total_asset', 'settle_up_time')
                ->where('stock_finance_id', $this->attributes['id'])->first();
            $arr['totalAssert'] = isset($rs['contract_total_asset']) ? round($rs['contract_total_asset'], 3) : 0;
            $arr['winLoss'] = 0;
            if (isset($rs['gain_loss'])) {
                $arr['winLoss'] = $rs['gain_loss'] ? round($rs['gain_loss_amount'] * -1, 3) : round($rs['gain_loss_amount'], 3);
            }
            $this->makeHidden('u_stock_finance_settleup');
        }
        return $this->attributes['stock_finance'] = $arr;
    }

    /**
     *  风险提示
     *
     * @param  string $value
     * @return string
     */
    public function getRiskNoticeAttribute()
    {
        if (!isset($this->attributes['stock_finance']['liquidationGap']) || !isset($this->attributes['stock_finance']['precautiousGap'])) {
            return $this->attributes['risk_notice'] = '未知';
        }
        if ($this->attributes['stock_finance']['liquidationGap'] <= 0) {
            return $this->attributes['risk_notice'] = '触平仓线 触预警线';
        }
        if ($this->attributes['stock_finance']['precautiousGap'] <= 0) {
            return $this->attributes['risk_notice'] = '触预警线';
        }
        return $this->attributes['risk_notice'] = '安全';
    }

    /**
     *  起初金额(总保证金+总配资额)
     *
     * @param  string $value
     * @return string
     */
    public function getTotalAmountAttribute()
    {
        return $this->attributes['init_caution_money'] + $this->attributes['post_finance_caution_money'] +
            $this->attributes['post_add_caution_money'] + $this->attributes['current_finance_amount'];
    }

    /**
     *  总保证金
     *
     * @param  string $value
     * @return string
     */
    public function getTotalCautionAttribute()
    {
        return $this->attributes['init_caution_money'] + $this->attributes['post_finance_caution_money'] +
            $this->attributes['post_add_caution_money'];
    }

    /**
     *  总冻结金额
     *
     * @param  string $value
     * @return string
     */
    public function getFreezeMoneyAttribute()
    {
        return $this->attributes['freeze_buying_money'] + $this->attributes['freeze_charge_money'];
    }


    /**
     *  状态
     *
     * @param  string $value
     * @return string
     */
    public function getStatusAttribute($value)
    {
        switch ($value) {
            case 1:
                return '操盘中';
            case 2:
                return '单向冻结';
            case 3:
                return '双向冻结';
            case 4:
                return '已结算';
            default:
                return '未知';
        }
    }
}
