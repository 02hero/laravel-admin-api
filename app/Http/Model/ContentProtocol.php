<?php

namespace App\Http\Model;

/**
 * Class ContentProtocol
 * @package App\Http\Model
 */
class ContentProtocol extends Base
{
    protected $table = "a_content_protocol";
    protected $guarded = ['id', 'create_time', 'updated_time'];
}
