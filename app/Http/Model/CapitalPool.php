<?php

namespace App\Http\Model;

class CapitalPool extends Base
{
    protected $table = "s_capital_pool";

    protected $guarded = ['id', 'created_time', 'updated_time'];

    public static function boot()
    {
        parent::boot();
        self::created(function ($model) {
            static::operate_log($model, '创建资金池');
        });
        self::updated(function ($model) {
            static::operate_log($model, '更新资金池', 1);
        });
    }

}
