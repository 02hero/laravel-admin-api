<?php

namespace App\Http\Model;

class UCustFullReport extends Base
{
    protected $table = "u_cust_full_report";

    protected $guarded = ['id', 'created_time', 'updated_time'];

    public function agent()
    {
        return $this->belongsTo(Agent::class, 'agent_id', 'id');
    }
}
