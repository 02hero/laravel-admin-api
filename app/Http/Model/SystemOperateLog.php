<?php

namespace App\Http\Model;

class SystemOperateLog extends Base
{
    protected $table = "s_system_operate_log";

    protected $guarded = ['id', 'created_time'];
}
