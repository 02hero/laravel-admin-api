<?php

namespace App\Http\Model;

/**
 * Class ContentArticle
 * @package App\Http\Model
 */
class ContentArticle extends Base
{
    protected $table = "a_content_article";
    protected $guarded = ['id', 'create_time', 'updated_time'];
}
