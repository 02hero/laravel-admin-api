<?php

namespace App\Http\Model;

use App\User;
use Illuminate\Support\Facades\Cache;


/**
 * App\Http\Model\Agent
 */
class Agent extends Base
{
    protected $table = "a_agent";
    protected $guarded = ['id', 'create_time', 'updated_time'];


    /**
     * 代理机构 拥有的登录用户
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function users()
    {
        return $this->hasMany(User::class, 'agent_id', 'id');
    }

    public function employees()
    {

        return $this->hasMany(Employee::class, 'agent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }


    public function info()
    {
        return $this->hasOne(AgentInfo::class, 'id', 'id');
    }

    public function percentages()
    {
        return $this->hasMany(AgentProfitRateConfig::class, 'agent_id', 'id');

    }

    /**
     * 获取自己和下级代理商
     * @param Agent $agent
     * @return mixed
     */
    public static function getAllChildrenAgentWithMyself(Agent $agent)
    {
        //缓存五分钟
        $list = Cache::remember('agent_children_list_recursive_' . $agent->id, 5, function () use ($agent) {
            $collections = collect([$agent]);
            $parentIds = [$agent->id];
            while (count($parentIds)) {
                $childrens = self::whereIn('parent_id', $parentIds)->get();
                if (count($childrens)) {
                    $collections = $collections->merge($childrens);
                    $parentIds = $childrens->pluck('id')->all();
                    $parentIds = array_values($parentIds);
                } else {
                    $parentIds = null;
                }
            }
            return $collections;
        });
        return $list;
    }

    /**
     * 获取自己和所有下级代理商(level_limit->1只获取1级 0无限极)
     * @param
     * @return array
     */
    public static function getAllChildrenAgents($parentAgentID, $level = '', $level_limit = 0, $only_children = false)
    {
        return \Cache::remember('AllChildrenAgents' . $parentAgentID . $level . $level_limit . $only_children,
            mt_rand(5, 10),
            function () use ($parentAgentID, $level, $level_limit, $only_children) {
                if (!$level) {
                    $level = self::getAgentLevel($parentAgentID);
                }
                if ($level > 4) {
                    return $only_children ? [] : [$parentAgentID];
                }
                $ids = [];
                for ($i = 0; $i < 6 - $level; $i++) {
                    $ChildrenAgentID = self::select('id', 'agent_level')->where('agent_level', ++$level)
                        ->whereIn('parent_id', (array)$parentAgentID)->pluck('id')->all();
                    if (!$ChildrenAgentID) {
                        break;
                    }
                    $ids = array_merge($ids, $ChildrenAgentID);
                    if ($level_limit == $i + 1) {
                        break;
                    }
                }
                if (!$only_children) {
                    array_unshift($ids, $parentAgentID);
                }
                return $ids;
            }
        );
    }

    /**
     * 获取代理商等级
     * @param
     * @return number
     */
    public static function getAgentLevel($parentAgentID)
    {
        return \Cache::remember('agent_level' . $parentAgentID, mt_rand(5, 10), function () use ($parentAgentID) {
            return self::select('agent_level')->find($parentAgentID)->agent_level;
        });
    }


}
