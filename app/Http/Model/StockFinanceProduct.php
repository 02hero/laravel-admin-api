<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class StockFinanceProduct extends Base
{
    //use SoftDeletes;不然关联的时候 一些历史记录找不到已删除的产品

    protected $table = "s_stock_finance_products";

    protected $guarded = ['id', 'deleted_at', 'created_time', 'updated_time'];

    protected $dates = ['deleted_at'];

    /**
     * belongsTo代理商
     */
    public function agent()
    {
        return $this->belongsTo(Agent::Class, 'agent_id', 'id');
    }

    public static function boot()
    {
        parent::boot();
        self::created(function ($model) {
            static::operate_log($model, '增加配资产品');
        });
        self::updated(function ($model) {
            static::operate_log($model, '更新配资产品', 1);
        });
    }
}
