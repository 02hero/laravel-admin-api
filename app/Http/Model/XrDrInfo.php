<?php

namespace App\Http\Model;

class XrDrInfo extends Base
{
    protected $table = "s_xr_dr_info";

    protected $guarded = ['id', 'created_time', 'updated_time'];

    /**
     *  状态
     *
     * @param  string $value
     * @return string
     */
    public function getXrDrStatusAttribute($value)
    {
        switch ($value) {
            case 0:
                return '未分配';
            case 1:
                return '已分配';
            default:
                return '未知';
        }
    }
}
