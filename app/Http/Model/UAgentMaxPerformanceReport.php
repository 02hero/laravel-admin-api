<?php

namespace App\Http\Model;

class UAgentMaxPerformanceReport extends Base
{
    protected $table = "u_agent_max_performance_report";

    /**
     * belongsTo代理商
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agent()
    {
        return $this->belongsTo(Agent::class, 'agent_id', 'id');
    }

    public function getMaxPerformanceAttribute($value)
    {
        return round($value, 3);
    }

    public function getTotalPerformanceAttribute($value)
    {
        return round($value, 3);
    }
}
