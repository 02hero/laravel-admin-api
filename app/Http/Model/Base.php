<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * App\Http\Model\Base
 *
 * @mixin \Eloquent
 */
class Base extends Model
{
    static $user = '';
    /**
     * 生成对应redis的key
     * @return string
     */
    public function GenerateCacheKey()
    {
        return $this->table . '.' . $this->id;
    }

    /**
     * 通过主键获取 redis数据
     * @param int $primaryId
     */
    public function getCachedModelBy(int $primaryId)
    {
        //TODO::
    }

    public static function boot()
    {
        parent::boot();

        static::$user = Auth::user();

        self::creating(function ($model) {
            // ... code here
        });

        self::created(function ($model) {
            // ... code here
            //TODO::创建redis
        });

        self::updating(function ($model) {
            // ... code here
        });

        self::updated(function ($model) {
            // ... code here
            //TODO::更新redis

        });

        self::deleting(function ($model) {
            // ... code here
        });

        self::deleted(function ($model) {
            // ... code here
            //TODO::销毁redis

        });
    }

    //写日志 flag=0时为创建或其他 flag=1时为更新 以后可扩展flag=2时为删除只记录删除id
    protected static function operate_log($model, $remark, $flag = 0)
    {
        $user = static::$user;
        $params = [
            'url' => \Route::currentRouteAction(),
            'ip' => request()->ip(),
            'sys_user' => $user->id,
            'sys_user_name' => $user->employee_name,
            'role_id' => $user->role_id,
            'remark' => $remark
        ];
        if ($flag) {
            //原参数
            $original_params = array_diff_assoc($model->original, $model->attributes);
            //更新后的参数
            $request_params = array_diff_assoc($model->attributes, $model->original);
            $params['request_params'] = json_encode($request_params);
            $params['original_params'] = json_encode($original_params);
        } else {
            $params['request_params'] = json_encode($model->attributes);
        }
        SystemOperateLog::create($params);
    }

    const CREATED_AT = "created_time";
    const UPDATED_AT = "updated_time";
}