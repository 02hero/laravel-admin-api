<?php

namespace App\Http\Model;

class UStockFinancingFlow extends Base
{
    protected $table = "u_stock_financing_flow";

    protected $guarded = ['id', 'created_time', 'updated_time'];

    /**
     * belongsTo客户
     */
    public function client()
    {
        return $this->belongsTo(Client::Class, 'cust_id', 'id');
    }

    /**
     * belongsTo子账户
     */
    public function u_stock_financing()
    {
        return $this->belongsTo(UStockFinancing::Class, 'stock_finance_id', 'id');
    }

    /**
     * belongsTo子账户委托
     */
    public function u_stock_finance_entrust()
    {
        return $this->belongsTo(UStockFinanceEntrust::Class, 'entrust_id', 'id');
    }

    /**
     * belongsTo子账户历史委托
     */
    public function u_stock_finance_entrust_history()
    {
        return $this->belongsTo(UStockFinanceEntrustHistory::Class, 'entrust_id', 'id');
    }

    /**
     * belongsTo联合委托
     */
    public function getUnionEntrustAttribute()
    {
        $this->attributes['union_entrust'] = $this->relations['u_stock_finance_entrust'] ?: $this->relations['u_stock_finance_entrust_history'];
        $this->makeHidden(['u_stock_finance_entrust', 'u_stock_finance_entrust_history']);
        return $this->attributes['union_entrust'];
    }

    /**
     *  记账类型
     *
     * @param  string $value
     * @return string
     */
    public function getAccountTypeAttribute($value)
    {
        switch ($value) {
            case 1:
                return '期初配资额';
            case 2:
                return '期初保证金';
            case 3:
                return '追加保证金';
            case 4:
                return '追配保证金';
            case 5:
                return '追加配资额';
            case 6:
                return '买入';
            case 7:
                return '买入';
            case 8:
                return '买入';
            case 9:
                return '买入';
            case 10:
                return '卖出';
            case 11:
                return '卖出';
            case 12:
                return '卖出';
            case 13:
                return '卖出';
            case 14:
                return '利润提取';
            case 15:
                return '配资结算';
            case 16:
                return '停牌回收';
            default:
                return '未知';
        }
    }
}
