<?php

namespace App\Http\Model;

/**
 * Class ContentHelpType
 * @package App\Http\Model
 */
class ContentHelpType extends Base
{
    protected $table = "a_content_help_type";
    protected $guarded = ['id', 'create_time', 'updated_time'];
}
