<?php

namespace App\Http\Model;

class UStockFinanceInterestPercentage extends Base
{
    protected $table = "u_stock_finance_interest_percentage";

    protected $guarded = ['id', 'created_time', 'updated_time'];

    /**
     * belongsTo子账户
     */
    public function u_stock_financing()
    {
        return $this->belongsTo(UStockFinancing::Class, 'stock_finance_id', 'id');
    }

    /**
     * belongsTo客户
     */
    public function client()
    {
        return $this->belongsTo(Client::Class, 'cust_id', 'id');
    }


    /**
     * belongsTo产品
     */
    public function product()
    {
        return $this->belongsTo(Client::Class, 'cust_id', 'id');
    }


    /**
     * 均价
     *
     * @param  string $value
     * @return string
     */
    public function getIsPaidOverAttribute($value)
    {
        if ($value == 1) return "缴纳完成";
        return "未缴纳完成";
    }

    /**
     * 业务类型
     *
     * @param  string $value
     * @return string
     */
    public function getInterestTypeAttribute($value)
    {
        switch ($value) {
            case 1:
                return '期初配资';
            case 2:
                return '追配';
            case 3:
                return '定时收息';
            default:
                return '未知';
        }
    }

    /**
     * 客户推广成本
     *
     * @param  string $value
     * @return string
     */
    public function getCustPromotionCostAttribute()
    {
        return $this->attributes['cust1_interests'] + $this->attributes['cust2_interests'] + $this->attributes['cust3_interests'];
    }
}
