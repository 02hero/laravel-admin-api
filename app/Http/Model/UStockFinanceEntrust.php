<?php

namespace App\Http\Model;

class UStockFinanceEntrust extends Base
{
    protected $table = "u_stock_finance_entrust";

    protected $guarded = ['id', 'created_time', 'updated_time'];

    /**
     * belongsTo客户
     */
    public function client()
    {
        return $this->belongsTo(Client::Class, 'cust_id', 'id');
    }

    /**
     * belongsTo子账户
     */
    public function u_stock_financing()
    {
        return $this->belongsTo(UStockFinancing::Class, 'stock_finance_id', 'id');
    }

    /**
     * hasOne母账户委托
     */
    public function u_parent_stock_finance_entrust()
    {
        return $this->hasOne(UParentStockFinanceEntrust::Class, 'stock_finance_entrust_id', 'id');
    }


    /**
     * hasMany成交
     */
    public function u_stock_finance_day_makedeal()
    {
        return $this->hasMany(UStockFinanceDayMakedeal::Class, 'stock_finance_entrust_id', 'id');
    }

    /**
     *  子账户委托状态
     *
     * @param  string $value
     * @return string
     */
    public function getStockFinanceEntrustStatusAttribute($value)
    {
        switch ($value) {
            case -1:
                return $this->attributes['cust_action'] == 3 ? '委托失败' : '委托等待返回';
            case 1:
                return '未成交';
            case 2:
                return '部分成交';
            case 3:
                return '部成部撤';
            case 4:
                return '已撤单';
            case 5:
                return '已成交';
            case 6:
                return '委托失败';
            default:
                return '未知';
        }
    }

    /**
     *  买卖方向
     *
     * @param  string $value
     * @return string
     */
    public function getSoldOrBuyAttribute($value)
    {
        switch ($value) {
            case 1:
                return '买入';
            case 2:
                return '卖出';
            default:
                return '未知';
        }
    }

}
