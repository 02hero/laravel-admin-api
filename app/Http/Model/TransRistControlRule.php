<?php

namespace App\Http\Model;

class TransRistControlRule extends Base
{
    protected $table = "s_trans_rist_control_rule";

    protected $guarded = ['id', 'created_time', 'updated_time'];


    /**
     *  格式化
     */
    public function getRiskControlValueAttribute($value)
    {
        switch ($this->attributes['risk_control_remark']) {
            case 'stock_sell_rate':
                return 100 * $value . '%';
            case 'stock_buy_rate':
                return 100 * $value . '%';
            case 'funding_stock_more_than':
                return 100 * $value . '%';
            default:
                return $value;
        }
    }
}
