<?php

namespace App\Http\Model;

class SystemParams extends Base
{
    protected $table = "s_system_params";

    protected $guarded = [];

    /**
     *  格式化
     */
    public function getParamValueAttribute($value)
    {
        switch ($this->attributes['param_key']) {
            case 'add_deposit_rate':
                return 100 * $value . '%';
            case 'cust1_rate':
                return 100 * $value . '%';
            case 'cust2_rate':
                return 100 * $value . '%';
            case 'cust3_rate':
                return 100 * $value . '%';
            default:
                return $value;
        }
    }
}
