<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Facades\Redis;

class StockInfo extends Base
{
    use SoftDeletes;

    protected $table = "s_stock_info";

    protected $guarded = ['id', 'deleted_at', 'created_time', 'updated_time'];

    protected $dates = ['deleted_at'];

    /**
     * 停牌天数
     *
     * @param  string $value
     * @return string
     */
    public function getHaltDaysAttribute()
    {
        if (!$this->attributes['trading_halt_time'] || !$this->attributes['resumption_time']) {
            return 0;
        }
        return (strtotime($this->attributes['resumption_time']) - strtotime($this->attributes['stock_code'])) / (24 * 3600);
    }

    public static function boot()
    {
        parent::boot();
        self::created(function ($model) {
            static::operate_log($model, '增加股票池');
        });
        self::updated(function ($model) {
            static::operate_log($model, '更新股票池', 1);
        });
    }

    public function getIsFollowParamAttribute($value)
    {
        switch ($value) {
            case 0:
                return '否';
                break;
            case 1:
                return '是';
                break;
            default:
                return '未知';
        }
    }

    public function getStockCategoryAttribute($value)
    {
        switch ($value) {
            case 1:
                return '上证';
                break;
            case 2:
                return '深证';
                break;
            default:
                return '未知';
        }
    }

    public function getIsStockEnableAttribute($value)
    {
        switch ($value) {
            case 0:
                return '否';
                break;
            case 1:
                return '是';
                break;
            default:
                return '未知';
        }
    }

    public function getHaltStatusAttribute($value)
    {
        $rs = json_decode(Redis::hGet("stockmarket", $this->attributes['stock_code']), true);
        if (!$rs) return '未知';
        return $rs['haltStatus'] ? '停牌' : '正常';
    }

    /**
     * 获取这个股票的信息  需要配合selectRaw('null as stock_info') 使用
     * @return mixed
     */
    public function getStockInfoAttribute()
    {
        return $this->attributes['stock_info'] = json_decode(Redis::hGet("stockmarket", $this->attributes['stock_code']), true);
    }


}
