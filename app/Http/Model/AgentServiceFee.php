<?php

namespace App\Http\Model;


/**
 * 代理商服务费分成
 * App\Http\Model\AgentServiceFee
 */
class AgentServiceFee extends Base
{
    public $timestamps = false;
    protected $table = "u_stock_finance_fee_report";
    protected $guarded = ['id', 'create_time', 'updated_time'];


    public function client()
    {
        return $this->belongsTo(Client::class, 'cust_id', 'id');
    }


    //

}
