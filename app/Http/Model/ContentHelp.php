<?php

namespace App\Http\Model;

/**
 * Class ContentHelp
 * @package App\Http\Model
 */
class ContentHelp extends Base
{
    protected $table = "a_content_help_article";
    protected $guarded = ['id', 'create_time', 'updated_time'];

    public function type()
    {
        return $this->belongsTo("App\Http\Model\ContentHelpType", "help_type", "id");
    }
}
