<?php

namespace App\Http\Model;

class UCustDayStockFinancing extends Base
{
    protected $table = "u_cust_day_stock_financing_report";

    protected $guarded = ['id', 'created_time', 'updated_time'];

    public function agent()
    {
        return $this->belongsTo(Agent::class, 'agent_id', 'id');
    }
}
