<?php

namespace App\Http\Model;

use Illuminate\Support\Facades\Redis;

class UStockFinanceHolding extends Base
{
    protected $table = "u_stock_finance_holdings";

    protected $guarded = ['id', 'created_time', 'updated_time'];

    /**
     * belongsTo股票
     */
    public function stock_info()
    {
        return $this->belongsTo(StockInfo::Class, 'stock_code', 'stock_code');
    }

    /**
     * belongsTo客户
     */
    public function client()
    {
        return $this->belongsTo(Client::Class, 'cust_id', 'id');
    }

    /**
     * 均价
     *
     * @param  string $value
     * @return string
     */
    public function getAvarageAttribute()
    {
        if (!$this->attributes['total_bought_amount']) {
            return 0;
        }
        $value = $this->attributes['total_bought_amount'] / ($this->attributes['total_sold_amount'] + $this->attributes['holdings_quantity']);
        return sprintf("%.3f", $value);
    }

    /**
     * 获取这个股票的信息  需要配合selectRaw('null as stock_info') 使用
     * @return mixed
     */
    public function getStockInfoAttribute()
    {
        return $this->attributes['stock_info'] = json_decode(Redis::hGet("stockmarket", $this->attributes['stock_code']), true);
    }

    /**
     * 获取持仓股票市值
     * @return mixed
     */
    public function getStockTotalAmountAttribute()
    {
        if (!isset($this->attributes['stock_info']['price'])) {
            return "";
        }
        return round($this->attributes['stock_info']['price'] * $this->attributes['holdings_quantity'], 3);
    }


}
