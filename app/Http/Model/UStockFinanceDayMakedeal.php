<?php

namespace App\Http\Model;

class UStockFinanceDayMakedeal extends Base
{
    protected $table = "u_stock_finance_day_makedeal";

    protected $guarded = ['id', 'created_time', 'updated_time'];

    /**
     * belongsTo客户
     */
    public function client()
    {
        return $this->belongsTo(Client::Class, 'cust_id', 'id');
    }

    /**
     * belongsTo子账户
     */
    public function u_stock_financing()
    {
        return $this->belongsTo(UStockFinancing::Class, 'stock_finance_id', 'id');
    }

    /**
     * belongsTo子账户委托
     */
    public function u_stock_finance_entrust()
    {
        return $this->belongsTo(UStockFinanceEntrust::Class, 'stock_finance_entrust_id', 'id');
    }

    /**
     * belongsTo母账户委托
     */
    public function u_parent_stock_finance_entrust()
    {
        return $this->belongsTo(UParentStockFinanceEntrust::Class, 'parent_entrust_id', 'id');
    }

    /**
     * belongsTo母账户成交
     */
    public function u_parent_stock_finance_day_makedeal()
    {
        return $this->belongsTo(UParentStockFinanceDayMakedeal::Class, 'parent_makedeal_id', 'id');
    }

    /**
     * 成交日期时间
     */
    public function getMakedealDateTimeAttribute()
    {
        return $this->attributes['makedeal_date_time'] = $this->attributes['makedeal_date'] . " " . $this->attributes['makedeal_time'];
    }

    /**
     *  成交状态
     *
     * @param  string $value
     * @return string
     */
    public function getMakeDealStatusAttribute($value)
    {
        switch ($value) {
            case 5:
                return '全部成交';
            case 2:
                return '部分成交';
            default:
                return '未知';
        }
    }

    /**
     *  买卖方向
     *
     * @param  string $value
     * @return string
     */
    public function getSellBuyAttribute($value)
    {
        switch ($value) {
            case 1:
                return '买入';
            case 2:
                return '卖出';
            default:
                return '未知';
        }
    }

}
