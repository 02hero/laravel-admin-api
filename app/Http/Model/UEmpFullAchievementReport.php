<?php

namespace App\Http\Model;


class UEmpFullAchievementReport extends Base
{
    protected $table = "u_emp_achievement_report";

    protected $guarded = ['id', 'created_time', 'updated_time'];


    public function employee()
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'id');
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class, 'agent_id', 'id');
    }

}
