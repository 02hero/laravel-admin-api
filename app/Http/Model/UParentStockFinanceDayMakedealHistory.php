<?php

namespace App\Http\Model;

class UParentStockFinanceDayMakedealHistory extends Base
{
    protected $table = "u_parent_stock_finance_day_makedeal_history";

    protected $guarded = ['id', 'created_time', 'updated_time'];

    /**
     * 成交日期时间
     */
    public function getMakedealDateTimeAttribute()
    {
        return $this->attributes['makedeal_date_time'] = $this->attributes['makedeal_date'] . " " . $this->attributes['makedeal_time'];
    }

    /**
     *  成交总价
     *
     * @param  string $value
     * @return string
     */
    public function getMakedealMoneyAttribute($value)
    {
        return round($value, 3);
    }
}
