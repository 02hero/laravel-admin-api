<?php

namespace App\Http\Controllers;

use Gregwar\Captcha\CaptchaBuilder;
use Gregwar\Captcha\PhraseBuilder;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use OSS\OssClient;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    const PAGE_SIZE = 15;
    const CAPTCHA_PREFIX = "captcha_";
    const CAPTCHA_CACHE = "redis";
    const CODE_SUCCESS = 1;
    const CODE_FAIL = 0;


    /**
     * 获取验证码 重新获取验证码
     * @param $captchaId ,$captchaCode
     * @return bool
     */
    static function verifyCaptchaCode($captchaId, $captchaCode): bool
    {
        $cacheKey = self::CAPTCHA_PREFIX . $captchaId;
        $cachedCode = Cache::store(self::CAPTCHA_CACHE)->get($cacheKey);
        //Cache::forget($cacheKey);
        return $cachedCode == $captchaCode;
    }

    /**
     * 设置图片验证码
     * @param $captchaId
     * @return string 返回图片base64 string
     */
    static function generateCaptchaImage($captchaId): string
    {
        $phraseBuilder = new PhraseBuilder(5, '0123456789');
        $builder = new CaptchaBuilder(null, $phraseBuilder);
        $builder->setDistortion(false);
        $builder->setIgnoreAllEffects(true);
        $builder->build();
        $cacheKey = self::CAPTCHA_PREFIX . $captchaId;
        Cache::store(self::CAPTCHA_CACHE)->put($cacheKey, $builder->getPhrase(), 5);
        return $builder->inline();
    }

    /**
     * @param array $data 返回json 数据体
     * @param int $code_status 返回 状态
     * @param string $message 消息
     * @param \Illuminate\Http\Request|null $request 请求 用于debug
     * @return \Illuminate\Http\JsonResponse  json返回
     */
    static function jsonReturn($data = [], int $code_status = self::CODE_SUCCESS, string $message = '', int $httpStatusCode = 200)
    {
        $json['status'] = $code_status ? 1 : 0;
        $json['data'] = $data;
        $json['msg'] = $message;
        if (config('app.debug')) {
            $json['debug_sql'] = DB::getQueryLog();
        }
        return response()->json($json, $httpStatusCode);
    }


    static function jsonPaginationReturn(Builder $query)
    {
        $page = request()->input('size', self::PAGE_SIZE);
        $data = $query->paginate($page);
        return self::jsonReturn($data);
    }

    static function generateCacheKeyByReqeust()
    {
        $request = request();
        $uri = $request->getUri();

        return $uri . '.' . http_build_query($request->all());
    }


    static function excelResponseReturn(Closure $call_back)
    {
        //https://github.com/PHPOffice/PHPExcel/tree/1.8/Examples
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setCreator("武汉映利科技有限公司")
            ->setLastModifiedBy("Eric Freeman Zhou")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml yingli gubao")
            ->setCategory("Test result file");
// Add some data
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Hello')
            ->setCellValue('B2', 'world!')
            ->setCellValue('C1', 'Hello')
            ->setCellValue('D2', 'world!');

// Miscellaneous glyphs, UTF-8
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A4', 'Miscellaneous glyphs')
            ->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');

// Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Simple');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="01simple.xlsx"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }

    /**
     * 阿里oss上传
     * @param $object
     * @param $content
     * @param string $type
     * @return bool|string
     */
    static function ossUpload($object, $content, $type = "")
    {
        $accessKeyId = "LTAI88B5zjDXRpPq";
        $accessKeySecret = " txb80snXCJrpBEnTupDlfd8Kbiw6k0";
        $endpoint = "http://oss-cn-shenzhen.aliyuncs.com";
        $bucket = "yingli";
        $content = $content;
        if ($type) {
            $object = $type . "/" . $object;
        }
        try {
            $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
            $ossClient->putObject($bucket, $object, $content);
        } catch (OssException $e) {
            return false;
        }

        return "http://yingli.oss-cn-shenzhen.aliyuncs.com/" . $object;
    }
}
