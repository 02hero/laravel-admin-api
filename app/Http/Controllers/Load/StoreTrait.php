<?php

namespace App\Http\Controllers\Load;

trait StoreTrait
{
    public function doStore($data = [])
    {
        $params = $data ?: request()->all();
        $model_path = 'App\Http\Model\\' . static::$model_name;
        return $params ? $model_path::create($params) : false;
    }
}