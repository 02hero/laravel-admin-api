<?php

namespace App\Http\Controllers\Load;

trait UpdateTrait
{
    public function doUpdate($id = '', $data = [])
    {
        if (!$id) return false;
        $model_path = 'App\Http\Model\\' . static::$model_name;
        $model = $model_path::query();
        static::initial_permission(static::$model_name);
        $model = static::_filter_orm_1(static::$permission, $model, static::$model_name);
        $params = $data ?: request()->all();
        return $model->find($id)->update($params);
    }
}