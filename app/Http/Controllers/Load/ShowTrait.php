<?php

namespace App\Http\Controllers\Load;

trait ShowTrait
{
    use ShowBaseTrait;

    public function index($data = [])
    {
        $params = $data ?: request()->all();
        $rs = static::_run_orm($params);
        return self::jsonReturn($rs);
    }

    public function show($id = '', $data = [])
    {
        $params = $data ?: request()->all();
        if ($id) $params['where']['id'] = $id;
        $rs = static::_run_orm($params, $id);
        return self::jsonReturn($rs);
    }
}