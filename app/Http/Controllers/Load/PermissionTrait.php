<?php

namespace App\Http\Controllers\Load;

use Illuminate\Support\Facades\Auth;
use App\Http\Model\Agent;
use App\Http\Model\ClientAgentEmployeeRelation;

trait PermissionTrait
{
    static $hidden_columns = [];
    static $permission = [];

    /**
     * 原理是根据权限 将模型查询语句先过滤一遍 从而只显示权限内的内容
     *
     * @param  \Illuminate\Http\Request $request
     * @param
     * @return mixed
     */
    static function initial_permission($model_name)
    {
        $user = Auth::user();
        if (in_array($user->role_id, [1, 2, 3, 4, 5])) {
            //如果是映利管理员
        } elseif (in_array($user->role_id, [11, 12, 13, 14, 15])) {
            static::$hidden_columns = ['remark', 'account_remark', 'parent_stock_finance_id', 'parent_entrust_id',
                'parent_makedeal_id', 'stock_entrust_code'];
            //如果是代理商
            if (in_array($model_name, ['UStockFinanceInterestPercentage'])) {
                $agent_level = Agent::getAgentLevel($user->agent_id);
                static::$permission['where']['agent' . $agent_level . '_id'] = $user->agent_id;
            } elseif (in_array($model_name, ['UStockFinancingFlow'])) {
                static::$permission['has']['u_stock_financing']['where']['agent_id'] = $user->agent_id;
            } elseif (config('select.' . $model_name . '.keyword') == 'agent_id') {
                $ids = Agent::getAllChildrenAgents($user->agent_id);
                static::$permission['whereIn']['agent_id'] = $ids;
            }
        } elseif ($user->role_id == 16) {
            static::$hidden_columns = ['remark', 'account_remark', 'parent_stock_finance_id', 'parent_entrust_id',
                'parent_makedeal_id', 'stock_entrust_code'];
            //如果是员工
            $ids = ClientAgentEmployeeRelation::getAllChildrenCusts($user->id);
            static::$permission['whereIn']['cust_id'] = $ids;
        }
        return;
    }
}
