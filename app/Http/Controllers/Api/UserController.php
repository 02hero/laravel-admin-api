<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Model\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:api");
    }

    /**
     * 后天管理员
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request)
    {
        $user = Auth::user();

        $query = User::orderByDesc('updated_time')->where('role_id', '<>', Role::ROLE_STAFF_AGENT)->with('agent', 'role');
        //如果不是系统管理员 而且 而且有代理商ID
        //只显示自己的员工

        $agent_id = $request->input('agent_id');
        if ($user->is_system_admin) {
            if ($agent_id) {
                $query->where(compact('agent_id'));
            } else {
                //显示全部
            }
        } else {
            //不是系统管理员就显示自己的客户
            $query->where('agent_id', $user->agent_id);
        }

        $keyword = $request->input('keyword');
        if ($keyword) {
            $query->orWhere('employee_name', 'like', "%$keyword%")->orWhere('phone', 'like', "%$keyword%")->orWhere('id', 'like', "%$keyword%");
        }
        $list = $query->paginate($request->input('size', self::PAGE_SIZE));

        return self::jsonReturn($list);
    }


    public function info(Request $request)
    {
        $user = Auth::user();

        $user->agent;
        $user->role;

        $last_ip = $request->ip();
        $user->update(compact('last_ip'));


        $user = $user->toArray();
        $user['my_ip'] = $last_ip;
        return compact('user', 'navs');
    }


    /**
     * 注销登陆
     */
    public function logoutApi()
    {
        if (Auth::check() && !config('app.debug')) {
            Auth::user()->AauthAcessToken()->delete();
        }
    }

    /**
     * 角色扮演发放token
     * @param Request $request
     */
    public function rolePlayIssueToken(Request $request)
    {
        $user = Auth::user();

        $agent_id = $request->agent_id;
        $employee_id = $request->employee_id;

        if ($employee_id) {
            //代理商员工
            $dummy = User::where('id', $employee_id)->whereNotIn('role_id', [Role::ROLE_ADMIN_SYSTEM, Role::ROLE_ADMIN_AGENT])->first();
        } else if ($agent_id && ($user->role_id == Role::ROLE_ADMIN_SYSTEM || $user->role_id == Role::ROLE_ADMIN_AGENT)) {
            //代理商扮演
            $role_id = Role::ROLE_ADMIN_AGENT;
            $dummy = User::where(compact('agent_id', 'role_id'))->first();

        } else {
            return self::jsonReturn([], 0, '您没有权限扮演用户');
        }
        if ($dummy) {
            $token = $dummy->createToken('role_play');
            return self::jsonReturn($token);
        } else {
            return self::jsonReturn([], 0, '不要在数据库里面直接添加代理商,或者修改角色');
        }

    }


}