<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Model\Client;
use App\Http\Model\UStockFinanceInterestPercentage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class AgentInterestController 代理商递延费
 * @package App\Http\Controllers\Api
 */
class AgentInterestController extends Controller
{


    public function __construct()
    {
        $this->middleware("auth:api");
    }


    public function list(Request $request)
    {
        $thisAgent = Auth::user()->agent;

        //汇总收益数据
        $agentLevel = intval($thisAgent->agent_level);
        $levelColumnName = "agent{$agentLevel}_id";
        $levelColumnNameInsterest = "agent{$agentLevel}_interests";
        $selectQuery = DB::raw("sum($levelColumnNameInsterest) as thisAgentAllInterest, sum(cust1_interests + cust2_interests + cust3_interests) as thisAgentPromoteCast");
        $statistic = UStockFinanceInterestPercentage::where($levelColumnName, $thisAgent->id)->where($levelColumnNameInsterest, '>', 0)->select($selectQuery)->first()->toArray();


        $query = UStockFinanceInterestPercentage::where($levelColumnName, $thisAgent->id)->where($levelColumnNameInsterest, '>', 0)->orderByDesc('id')->with('client', 'u_stock_financing.stock_finance_product');
        $stockAccountId = $request->stockAccountId;
        if ($stockAccountId) {
            $query->where('stock_finance_id', 'like', "%$stockAccountId%");
        }
        $phone = $request->phone;
        if ($phone) {
            $clientIDS = Client::where('cellphone', 'like', "%$phone%")->pluck('id')->toArray();
            $query->whereIn('cust_id', array_values($clientIDS));
        }
        //过滤条件 是否支付完成
        $is_paid_over = $request->is_paid_over;
        if ($is_paid_over !== null) {
            $query->where(compact('is_paid_over'));
        }


        $per_page = $request->input('size', self::PAGE_SIZE);
        $data = $query->paginate($per_page)->toArray();

        foreach ($data['data'] as $key => $vo) {
            $data['data'][$key]['myShareInterest'] = $vo[$levelColumnNameInsterest];
        }

        $data = array_merge($data, $statistic);
        return self::jsonReturn($data);
    }


}