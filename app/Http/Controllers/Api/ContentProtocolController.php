<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Model\ContentProtocol;
use App\Http\Model\Role;
use Illuminate\Http\Request;

class ContentProtocolController extends Controller
{
    private $protocolTypes = [
        ["id"=>1, "name"=>"风险揭示书"],
        ["id"=>2, "name"=>"合格投资人申明"],
        ["id"=>3, "name"=>"平台注册协议"],
        ["id"=>4, "name"=>"隐私条款"],
    ];

    public function __construct()
    {
        $this->middleware("auth:api")->except(['search']);
    }

    /**
     * 协议列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function protocols(Request $request)
    {
        $user = $request->user();
        $per_page = $request->input('size', self::PAGE_SIZE);
        $keyword = $request->input('keyword');
        $query = ContentProtocol::orderBy('created_time', 'desc');
        if ($keyword) {
            $query->orWhere('name', 'like', "%$keyword%")
                ->orWhere('id', 'like', "%$keyword%")
                ->orWhere("agent_id", "like", "%$keyword%");
        }

        //代理商
        if ($user->role_id != Role::ROLE_ADMIN_SYSTEM) {
            $query->where("agent_id", $user->agent_id);
        }

        $data = $query->paginate($per_page);
        return self::jsonReturn($data);
    }

    /**
     * 获取协议详情
     * @param Request $request
     */
    public function getProtocolInfo(Request $request)
    {
        $user = \Auth::user();
        $query = ContentProtocol::where('id', $request->get("id"));
        //代理商
        if ($user->role_id != Role::ROLE_ADMIN_SYSTEM) {
            $query->where("agent_id", $user->agent_id);
        }
        $ret = $query->first();
        return  self::jsonReturn($ret);
    }

    /**
     * 协议类型列表
     * @return \Illuminate\Http\JsonResponse
     */
    public function protocolTypes()
    {
        return self::jsonReturn($this->protocolTypes);
    }

    /**
     * 编辑协议
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function protocolEdit(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'type' => 'required|integer|between:1,4',
        ], [
            "协议类别错误",
        ]);

        if ($validator->fails()) {
            return parent::jsonReturn([], parent::CODE_FAIL, $validator->errors()->first());
        }

        $user = \Auth::user();
        $protocolName = (string)$this->getProtocolName($request->get("type"));
        $data = array_merge($request->only(["type", "content"]), [
            "name"=>$protocolName
        ] );
        foreach ($data as $k=>$v) {
            if ($v === null) $data[$k] = "";
        }

        $id = $request->get("id");
        $hasCreateSameProtocolQuery = ContentProtocol::where("type", $request->get("type"));
        if ($id) {
            $protocolQuery = ContentProtocol::where("id", $id);
            if ($user->role_id != Role::ROLE_ADMIN_SYSTEM) {
                $protocolQuery->where("agent_id", $user->agent_id);
            }
            $protocol = $protocolQuery->first();
            if (!$protocol) return self::jsonReturn([], self::CODE_FAIL, "信息错误");

            $hasCreateSameProtocol = $hasCreateSameProtocolQuery->where("id", "!=", $id)
                ->where("agent_id", $protocol->agent_id)->count();
            if ($hasCreateSameProtocol) {
                return self::jsonReturn([], self::CODE_FAIL, "该代理商已创建此协议，无法再创建");
            }

            $ret = $protocol->update($data);
        } else {
            $hasCreateSameProtocolQuery->where("agent_id", $user->agent_id);
            $hasCreateSameProtocol = $hasCreateSameProtocolQuery->count();
            if ($hasCreateSameProtocol) {
                return self::jsonReturn([], self::CODE_FAIL, "该代理商已创建此协议，无法再创建");
            }

            $data = array_merge($data, [
                "agent_id"=>$user->agent_id,
                "employee_id"=>$user->id,
            ] );
            $ret = ContentProtocol::create($data);
        }

        return $ret ? self::jsonReturn([], self::CODE_SUCCESS, "操作成功") :
            self::jsonReturn([], self::CODE_FAIL, "操作失败");
    }

    /**
     * 删除协议
     * @param Request $request
     */
    public function protocolDelete(Request $request)
    {
        $user = \Auth::user();
        $query = ContentProtocol::where('id', $request->get("id"));
        //代理商
        if ($user->role_id != Role::ROLE_ADMIN_SYSTEM) {
            $query->where("agent_id", $user->agent_id);
        }
        $protocol = $query->first();
        if (!$protocol) return self::jsonReturn([], self::CODE_FAIL, "该协议不存在");

        $ret = $protocol->delete();
        return self::jsonReturn([]);
    }

    /**
     * 获取协议名称
     * @param $type
     * @return null
     */
    private function getProtocolName($type)
    {
        foreach ($this->protocolTypes as $v) {
            if ($v["id"] == $type) return $v["name"];
        }

        return null;
    }
}