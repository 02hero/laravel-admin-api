<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Model\AgentServiceFee;
use App\Http\Model\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class AgentServiceFeeController 代理商服务费控制器
 * @package App\Http\Controllers\Api
 */
class AgentServiceFeeController extends Controller
{


    public function __construct()
    {
        $this->middleware("auth:api");
    }

    public function list(Request $request)
    {
        $agent = Auth::user()->agent;
        $whereKey = "agent{$agent->agent_level}_id";
        $agentWhere[$whereKey] = $agent->id;

        $keyword = $request->input('keyword');

        $query = AgentServiceFee::orderByDesc('id')->where($agentWhere)->with('client');

        if ($keyword) {
            //搜索用户手机号
            //搜索子账号ID
            $clientIDs = Client::where('cellphone', 'like', "%$keyword%")->pluck('id')->all();
            $query->where(function ($subQ) use ($clientIDs, $keyword) {
                $subQ->orWhereIn('cust_id', array_values($clientIDs))->orWhere('stock_finance_id', 'like', "%$keyword%");
            });
        }


        //根据level获取这个一级代理应该分的佣金
        $myFeeKey = "agent{$agent->agent_level}_fee";

        $per_page = $request->input('size', self::PAGE_SIZE);
        $list = $query->paginate($per_page)->toArray();
        foreach ($list['data'] as $key => $item) {
            $list['data'][$key]['myFee'] = $item[$myFeeKey];
        }
        return self::jsonReturn($list);
    }


}