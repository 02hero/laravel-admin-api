<?php

namespace App\Http\Controllers\Api\System;

use App\Http\Controllers\Controller;
use App\Http\Model\UStockFinanceHolding;
use Illuminate\Http\Request;

class UStockFinanceHoldingController extends Controller
{
    use \App\Http\Controllers\Load\ShowTrait, \App\Http\Controllers\Load\UpdateTrait, \App\Http\Controllers\Load\StoreTrait;

    public static $model_name = 'UStockFinanceHolding';

    public function __construct()
    {
        $this->middleware(["auth:api", "auth.request"]);
    }

    public function stock_finance_index($stock_finance_id = '', $data = [])
    {
        $agent_id = \Auth::user()->agent_id;
        if (!$agent_id || !$stock_finance_id) return;
        $params = $data ?: request()->all();
        $params['where']['stock_finance_id'] = $stock_finance_id;
        $rs = static::_run_orm($params);
        return self::jsonReturn($rs);
    }

    /**
     * 管理员-持仓股份菜单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function holdingStockList(Request $request)
    {
        $per_page = $request->input('size', self::PAGE_SIZE);
        $keyword = $request->input('keyword');
        $query = UStockFinanceHolding::selectRaw('id,stock_finance_id,parent_stock_finance_id,stock_code,stock_name,cust_id, total_sold_amount,holdings_quantity, available_sell_quantity, is_recycle, total_bought_amount, null as avarage,null as stock_info')->with('client');
        if ($keyword) {
            $query->orWhere('stock_finance_id', 'like', "%$keyword%")->orWhere('stock_code', 'like', "%$keyword%")->orWhere('stock_name', 'like', "%$keyword%");
        }
        //增加过滤条件
        $stock_finance_id = $request->stock_finance_id;
        if ($stock_finance_id) {
            $query->where(compact('stock_finance_id'));
        }
        $data = $query->paginate($per_page);
        return self::jsonReturn($data);
    }

    public function update($id = '', $data = [])
    {
        $params = $data ?: request()->all();
        $rs = $this->doUpdate($id, $params);
        if ($rs) return self::jsonReturn([], 1, '更新成功！');
        return self::jsonReturn([], 0, '更新失败！');
    }
}