<?php

namespace App\Http\Controllers\Api\System;

use App\Http\Controllers\Controller;

class UStockFinanceEntrustHistoryController extends Controller
{
    use \App\Http\Controllers\Load\ShowTrait;

    public static $model_name = 'UStockFinanceEntrustHistory';

    public function __construct()
    {
        $this->middleware(["auth:api", "auth.request"]);
    }
}