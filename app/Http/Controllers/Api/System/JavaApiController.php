<?php

namespace App\Http\Controllers\Api\System;

use App\Http\Controllers\Controller;
use App\Http\Model\RequestJavaApiLog;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class JavaApiController Java交互控制器
 * @package App\Http\Controllers\Api
 */
class JavaApiController extends Controller
{
    public function __construct()
    {
        $this->middleware(["auth:api", "auth.request"]);
    }

    //子账户一键平仓
    public function eveningUp(Request $request)
    {
        //TODO::用户权限验证
        $uri = 'stockfinance/eveningup';
        $params = [
            'stockFinanceId' => $request->stockFinanceId,
            'entrustIp' => $request->ip()
        ];
        return $this->sendJavaRequestByGuzzleHttp($uri, $params);
    }

    //子账户分笔平仓
    public function eveningUpPerHolding(Request $request)
    {
        $uri = 'stockfinance/eveningupPerHolding';
        $params = [
            'id' => $request->id,
            'entrustIp' => $request->ip()
        ];
        return $this->sendJavaRequestByGuzzleHttp($uri, $params);
    }


    //手动派股票 手动派股
    public function deliveryStock(Request $request)
    {
        //TODO::用户权限验证  写日志
        $uri = 'deliveryStock';
        $params = $request->only('stockFinanceHoldId', 'deliveryStockAmount');
        return $this->sendJavaRequestByGuzzleHttp($uri, $params);
    }

    //手动除权除息
    public function doXrdr(Request $request)
    {
        $uri = 'doXrdr';
        $params = [
            'id' => $request->id,
        ];
        return $this->sendJavaRequestByGuzzleHttp($uri, $params);
    }

    //回收股票
    public function retrieveStock(Request $request)
    {
        $uri = 'retrieveStock';
        $params = $request->only('stockFinanceHoldId', 'marketPrice', 'retrievePercentage');
        return $this->sendJavaRequestByGuzzleHttp($uri, $params);
    }

    //调母账户余额
    public function adjustBalance(Request $request)
    {
        $uri = 'parentStockFinance/adjustBalance';
        $params = [
            'parentStockFinanceId' => $request->parentStockFinanceId,
            'offsetAmount' => $request->offsetAmount,
        ];
        return $this->sendJavaRequestByGuzzleHttp($uri, $params);
    }


    //买入交易关联当日子账户
    public function assignBuyDayMakeDeal(Request $request)
    {
        $uri = 'assignBuyDayMakeDeal';
        $params = [
            'id' => $request->id,
            'stockFinanceEntrustId' => $request->stockFinanceEntrustId
        ];
        return $this->sendJavaRequestByGuzzleHttp($uri, $params);
    }

    //买入交易关联历史子账户
    public function assignBuyHistoryMakeDeal(Request $request)
    {
        $uri = 'assignBuyHistoryMakeDeal';
        $params = [
            'id' => $request->id,
            'stockFinanceEntrustId' => $request->stockFinanceEntrustId
        ];
        return $this->sendJavaRequestByGuzzleHttp($uri, $params);
    }


    //卖出交易关联当日子账户
    public function assignSellDayMakeDeal(Request $request)
    {
        $uri = 'assignSellDayMakeDeal';
        $params = [
            'id' => $request->id,
            'stockFinanceId' => $request->stockFinanceId,
            'remark' => $request->remark
        ];
        return $this->sendJavaRequestByGuzzleHttp($uri, $params);
    }

    //卖出交易关联历史子账户
    public function assignSellHistoryMakeDeal(Request $request)
    {
        $uri = 'assignSellHistoryMakeDeal';
        $params = [
            'id' => $request->id,
            'stockFinanceId' => $request->stockFinanceId,
            'remark' => $request->remark
        ];
        return $this->sendJavaRequestByGuzzleHttp($uri, $params);
    }

    //结算
    public function settleup(Request $request)
    {
        $uri = 'stockfinance/settleup';
        $params = [
            'stockFinanceId' => $request->stockFinanceId
        ];
        return $this->sendJavaRequestByGuzzleHttp($uri, $params);
    }

    /**
     * guzzle http post 数据到java 客户端
     * http://docs.guzzlephp.org/en/stable/quickstart.html
     * @param $uri  前面不要带斜线 base_url=http://foo.com/foo/	uri=bar	 结果:http://foo.com/foo/bar
     * @param $params
     * @return \Illuminate\Http\JsonResponse
     */
    private function sendJavaRequestByGuzzleHttp($uri, $params)
    {
        //TODO::部署的时候需要注意一下更换IP地址
        $guzzleClient = new Client([
            'base_uri' => config('app.java_api_base_url'),
            'timeout' => 2.0,
        ]);
        $response = $guzzleClient->request('POST', $uri, [
            'form_params' => $params
        ]);
        try {
            //这样写配合后置中间件路由
            $data = \GuzzleHttp\json_decode($response->getBody());
            if ($response->getStatusCode() == 200) {
                //写日志到数据库
                $user = Auth::user();
                RequestJavaApiLog::create([
                    'url' => $uri,
                    'params' => json_encode($params),
                    'ip' => request()->ip(),
                    'sys_user' => $user->id,
                    'sys_user_name' => $user->employee_name,
                    'role_id' => $user->role_id
                ]);
            }
            return response()->json($data, $response->getStatusCode());
        } catch (\Exception $e) {
            return self::jsonReturn([], 0, $e->getMessage());
        }
    }

    // trade/api/1.0/stockfinance/repay  穿仓与利息抵帐
    // 这个是在php代码里面内部条用不需要 映射到路由
    public function StockFinanceRepay($custId, $availableAmount)
    {
        //http://10.10.1.101:8000/finances-center/trade/api/1.0/stockfinance/repay?custId=111111&availableAmount=111
        $uri = '/finances-center/trade/api/1.0/stockfinance/repay';
        return $this->sendJavaRequestByGuzzleHttp($uri, compact('custId', 'availableAmount'));
    }
}