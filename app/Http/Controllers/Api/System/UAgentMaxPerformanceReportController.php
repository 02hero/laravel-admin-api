<?php

namespace App\Http\Controllers\Api\System;

use App\Http\Controllers\Controller;
use App\Http\Model\Agent;
use App\Http\Model\UAgentMaxPerformanceReport;

class UAgentMaxPerformanceReportController extends Controller
{
    use \App\Http\Controllers\Load\ShowTrait;

    public static $model_name = 'UAgentMaxPerformanceReport';

    public function __construct()
    {
        $this->middleware(["auth:api", "auth.request"]);
    }

    public function main_index($data = [])
    {
        $params = $data ?: request()->all();
        $user = \Auth::user();
        if (in_array($user->role_id, [1, 2, 3, 4, 5])) {
            $children_list = Agent::where('agent_level', 1)->orderBy('id')->pluck('id')->all();
            $rs['props']['sum'] = UAgentMaxPerformanceReport::selectRaw('"合计" as agent_id,sum(max_performance+sub_agent_max_performance) as total_performance,sum(max_performance) as max_performance')
                ->whereBetween('occur_time', $params['whereBetween']['occur_time'])->where('agent_level', 1)->first();
        } else {
            $children_list = Agent::getAllChildrenAgents($user->agent_id, '', 1, false);
        }
        if (!$children_list) return self::jsonReturn();
        $model = UAgentMaxPerformanceReport::whereIn('agent_id', $children_list)
            ->whereBetween('occur_time', $params['whereBetween']['occur_time'])
            ->groupBy('agent_id')
            ->select('agent_id');
        if (!empty($params['count'])) {
            $sql = $model->toSql();
            $rs['count'] = \DB::select('select count(*) as num from (' . $sql . ')a'
                , array_merge((array)$children_list, $params['whereBetween']['occur_time']))[0]->num;
        }
        if (!empty($params['limit'])) $model->limit($params['limit'])->offset($params['offset']);
        $rs['list'] = $model->selectRaw('            
            sum(max_performance+sub_agent_max_performance) as total_performance,
            sum(max_performance) as max_performance
            ')->with(['agent' => function ($query) {
            $query->select('id', 'agent_level', 'agent_name', 'agent_number', 'parent_id');
        }])
            ->orderByRaw('if(agent_id=' . $user->agent_id . ',0,1),agent_id asc')->get();

        return self::jsonReturn($rs);
    }

    public function children_index($data = [])
    {
        $params = $data ?: request()->all();
        if (empty($params['where']['agent_id'])) {
            return self::jsonReturn();
        }
        $children_list = Agent::getAllChildrenAgents($params['where']['agent_id'], '', 1, true);
        if (!$children_list) return self::jsonReturn();
        $model = UAgentMaxPerformanceReport::whereIn('agent_id', $children_list)
            ->whereBetween('occur_time', $params['whereBetween']['occur_time'])
            ->groupBy('agent_id')
            ->select('agent_id');
        if (!empty($params['count'])) {
            $sql = $model->toSql();
            $rs['count'] = \DB::select('select count(*) as num from (' . $sql . ')a'
                , array_merge((array)$children_list, $params['whereBetween']['occur_time']))[0]->num;
        }
        if (!empty($params['limit'])) $model->limit($params['limit'])->offset($params['offset']);
        $rs['list'] = $model->selectRaw('
            sum(max_performance+sub_agent_max_performance) as total_performance,
            sum(max_performance) as max_performance
            ')->with(['agent' => function ($query) {
            $query->select('id', 'agent_level', 'agent_name', 'agent_number', 'parent_id');
        }])
            ->orderByRaw('agent_id asc')->get();
        return self::jsonReturn($rs);
    }

}