<?php

namespace App\Http\Controllers\Api\System;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class UStockFinancingController extends Controller
{
    use \App\Http\Controllers\Load\ShowTrait, \App\Http\Controllers\Load\UpdateTrait, \App\Http\Controllers\Load\StoreTrait;

    public static $model_name = 'UStockFinancing';

    public function __construct()
    {
        $this->middleware(["auth:api", "auth.request"]);
    }

    public function riskList($data = [])
    {
        $params = $data ?: request()->all();
        /*if (empty($params['search']['id']) && empty($params['where']['status'])
            && empty($params['has']['client']['search']['cellphone'])
        ) {
            if ($params['current_type'] == 1) {
                $ids = Redis::zRange('liquidationsIndex', $params['offset'], $params['limit'] + $params['offset'] - 1);
            } else {
                $ids = Redis::zRange('precautionsIndex', $params['offset'], $params['limit'] + $params['offset'] - 1);
            }
            $params['offset'] = 0;
            $field = implode(',', array_reverse($ids));//反序 下面desc 负负得正~  order by field desc比asc要保险！
            $params['order'] = 'field(id,' . $field . ') desc';
        } else { */
        if ($params['current_type'] == 1) {
            $ids = Redis::zRange('liquidationsIndex', 0, -1);
        } else {
            $ids = Redis::zRange('precautionsIndex', 0, -1);
        }
        $field = implode(',', array_reverse($ids));//反序 下面desc 负负得正~  order by field desc比asc要保险！
        $params['order'] = 'field(id,' . $field . ') desc';
        /*}*/
        $rs = static::_run_orm($params);
        return self::jsonReturn($rs);
    }

    public function update($id = '', $data = [])
    {
        $params = $data ?: request()->only(['status', 'capital_pool_id', 'liiquidation_line_amount', 'precautious_line_amount']);
        static::$permission['whereIn'] = ['status' => [1, 2, 3]];
        $rs = $this->doUpdate($id, $params);
        if ($rs) return self::jsonReturn([], 1, '更新成功！');
        return self::jsonReturn([], 0, '更新失败！');
    }
}