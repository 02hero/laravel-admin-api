<?php

namespace App\Http\Controllers\Api\System;

use App\Http\Controllers\Controller;

class UStockFinanceEntrustController extends Controller
{
    use \App\Http\Controllers\Load\ShowTrait;

    public static $model_name = 'UStockFinanceEntrust';

    public function __construct()
    {
        $this->middleware(["auth:api", "auth.request"]);
    }
}