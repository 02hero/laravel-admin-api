<?php

namespace App\Http\Controllers\Api\System;

use App\Http\Controllers\Controller;

class UStockFinanceDayMakedealHistoryController extends Controller
{
    use \App\Http\Controllers\Load\ShowTrait;

    public static $model_name = 'UStockFinanceDayMakedealHistory';

    public function __construct()
    {
        $this->middleware(["auth:api", "auth.request"]);
    }
}