<?php

namespace App\Http\Controllers\Api\System;

use App\Http\Controllers\Controller;
use App\Http\Model\SystemParams;

/**
 * Class SystemController 系统代理商
 * @package App\Http\Controllers\Api
 */
class SystemParamsController extends Controller
{
    //use \App\Http\Controllers\Load\ShowTrait, \App\Http\Controllers\Load\UpdateTrait;

    public static $model_name = 'SystemParams';

    public function __construct()
    {
        $this->middleware(["auth:api", "auth.request"]);
    }

    public function index()
    {
        $agent_id = array_unique([\Auth::user()->agent_id, 0]);
        $rs['list'] = SystemParams::whereIn('agent_id', $agent_id)->get()->keyBy('param_key')->sortBy('param_type');
        return self::jsonReturn($rs);
    }

    public function store($data = [])
    {
        $params = $data ?: request()->all();
        $rs = $this->doStore($params);
        if ($rs) return self::jsonReturn([], 1, '新增成功！');
        return self::jsonReturn([], 0, '新增失败！');
    }
}