<?php

namespace App\Http\Controllers\Api\System;

use App\Http\Controllers\Controller;
use App\Http\Model\Agent;
use Illuminate\Support\Facades\Auth;

class UStockFinanceInterestPercentageController extends Controller
{
    use \App\Http\Controllers\Load\ShowBaseTrait, \App\Http\Controllers\Load\UpdateTrait, \App\Http\Controllers\Load\StoreTrait;

    public static $model_name = 'UStockFinanceInterestPercentage';
    protected $level = '';
    protected $user = '';

    public function __construct()
    {
        $this->middleware(["auth:api", "auth.request"]);
    }

    public function index($data = [])
    {
        $params = $data ?: request()->all();
        $params['field'] = $params['field'] ?: [];
        $arr = [];
        $level = Agent::getAgentLevel(Auth::user()->agent_id);
        for ($i = 1; $i < $level; $i++) {
            $arr[] = 'agent' . $i . '_id';
            $arr[] = 'agent' . $i . '_interests';
        }
        $params['field'] = array_diff($params['field'], $arr);
        array_push($params['field'], '(
                CASE
                WHEN (agent5_id IS NOT NULL and agent5_id !=0) THEN
                  agent5_id
                WHEN (agent4_id IS NOT NULL and agent4_id !=0) THEN
                  agent4_id
                WHEN (agent3_id IS NOT NULL and agent3_id !=0) THEN
                  agent3_id
                WHEN (agent2_id IS NOT NULL and agent2_id !=0) THEN
                  agent2_id
                WHEN (agent1_id IS NOT NULL and agent1_id !=0) THEN
                  agent1_id
                END
            ) agent_id');
        $rs = static::_run_orm($params);
        $rs['props']['agent_level'] = $level;
        return self::jsonReturn($rs);
    }

    public function stock_finance_index($stock_finance_id = '', $data = [])
    {
        if (!$stock_finance_id) return;
        $params = $data ?: request()->all();
        $params['where']['stock_finance_id'] = $stock_finance_id;
        $rs = static::_run_orm($params);
        return self::jsonReturn($rs);
    }

    public function update($id = '', $data = [])
    {
        $params = $data ?: request()->all();
        $rs = $this->doUpdate($id, $params);
        if ($rs) return self::jsonReturn([], 1, '更新成功！');
        return self::jsonReturn([], 0, '更新失败！');
    }
}