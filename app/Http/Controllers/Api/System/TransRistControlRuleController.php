<?php

namespace App\Http\Controllers\Api\System;

use App\Http\Controllers\Controller;

class TransRistControlRuleController extends Controller
{
    use \App\Http\Controllers\Load\ShowTrait, \App\Http\Controllers\Load\UpdateTrait, \App\Http\Controllers\Load\StoreTrait;

    public static $model_name = 'TransRistControlRule';

    public function __construct()
    {
        $this->middleware(["auth:api", "auth.request"]);
    }

    public function update($id = '', $data = [])
    {
        $params = $data ?: request()->all();
        $rs = $this->doUpdate($id, $params);
        if ($rs) return self::jsonReturn([], 1, '更新成功！');
        return self::jsonReturn([], 0, '更新失败！');
    }
}