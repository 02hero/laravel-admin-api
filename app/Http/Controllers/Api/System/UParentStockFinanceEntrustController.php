<?php

namespace App\Http\Controllers\Api\System;

use App\Http\Controllers\Controller;

class UParentStockFinanceEntrustController extends Controller
{
    use \App\Http\Controllers\Load\ShowTrait;

    public static $model_name = 'UParentStockFinanceEntrust';

    public function __construct()
    {
        $this->middleware(["auth:api", "auth.request"]);
    }

}