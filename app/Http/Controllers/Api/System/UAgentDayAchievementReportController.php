<?php

namespace App\Http\Controllers\Api\System;

use App\Http\Controllers\Controller;
use App\Http\Model\Agent;
use App\Http\Model\UAgentDayAchievementReport;

class UAgentDayAchievementReportController extends Controller
{
    use \App\Http\Controllers\Load\ShowTrait;

    public static $model_name = 'UAgentDayAchievementReport';
    protected $select = 'sum(direct_cust_day_incharge+home_cust_day_incharge) as total_incharge,sum(direct_cust_day_incharge) as direct_cust_day_incharge,sum(direct_cust_day_cash+home_cust_day_cash) as total_cash,sum(direct_cust_day_cash) as direct_cust_day_cash,sum(direct_cust_day_stock_finance+home_cust_day_stock_finance) as total_stock_finance,sum(direct_cust_day_stock_finance) as direct_cust_day_stock_finance,sum(direct_cust_day_make_deal+home_cust_dayl_make_deal) as total_make_deal,sum(direct_cust_day_make_deal) as direct_cust_day_make_deal,sum(direct_cust_day_fee+home_cust_day_fee) as total_fee,sum(direct_cust_day_fee) as direct_cust_day_fee,sum(direct_cust_day_interests+home_cust_day_interests) as total_interests,sum(direct_cust_day_interests) as direct_cust_day_interests,sum(direct_cust_day_interests_percentage+home_cust_day_interests_percentage) as total_interests_percentage,sum(direct_cust_day_commision+home_cust_day_commision) as total_commision,sum(direct_cust_day_commision) as direct_cust_day_commision,sum(direct_cust_day_commision_percentage+home_cust_day_commision_percentage) as total_commision_percentage,sum(direct_cust_adv_day_cost+home_cust_adv_day_cost) as total_cost,sum(direct_cust_adv_day_cost) as direct_cust_adv_day_cost';

    public function __construct()
    {
        $this->middleware(["auth:api", "auth.request"]);
    }

    public function main_index($data = [])
    {
        $params = $data ?: request()->all();
        $user = \Auth::user();
        if (in_array($user->role_id, [1, 2, 3, 4, 5])) {
            $children_list = Agent::where('agent_level', 1)->orderBy('id')->pluck('id')->all();
            $rs['props']['sum'] = UAgentDayAchievementReport::selectRaw('"合计" as agent_id,sum(direct_cust_day_incharge+home_cust_day_incharge) as total_incharge,sum(direct_cust_day_cash+home_cust_day_cash) as total_cash,sum(direct_cust_day_stock_finance+home_cust_day_stock_finance) as total_stock_finance,sum(direct_cust_day_make_deal+home_cust_dayl_make_deal) as total_make_deal,sum(direct_cust_day_fee+home_cust_day_fee) as total_fee,sum(direct_cust_day_interests+home_cust_day_interests) as total_interests,sum(direct_cust_day_interests_percentage+home_cust_day_interests_percentage) as total_interests_percentage,sum(direct_cust_day_commision+home_cust_day_commision) as total_commision,sum(direct_cust_day_commision_percentage+home_cust_day_commision_percentage) as total_commision_percentage,sum(direct_cust_adv_day_cost+home_cust_adv_day_cost) as total_cost')
                ->whereBetween('occur_time', $params['whereBetween']['occur_time'])->where('agent_level', 1)->first();
        } else {
            $children_list = Agent::getAllChildrenAgents($user->agent_id, '', 1, false);
        }
        if (!$children_list) return self::jsonReturn();
        $model = UAgentDayAchievementReport::whereIn('agent_id', $children_list)
            ->whereBetween('occur_time', $params['whereBetween']['occur_time'])
            ->groupBy('agent_id')
            ->select('agent_id');
        if (!empty($params['count'])) {
            $sql = $model->toSql();
            $rs['count'] = \DB::select('select count(*) as num from (' . $sql . ')a'
                , array_merge((array)$children_list, $params['whereBetween']['occur_time']))[0]->num;
        }
        if (!empty($params['limit'])) $model->limit($params['limit'])->offset($params['offset']);
        $rs['list'] = $model->selectRaw($this->select)->with(['agent' => function ($query) {
            $query->select('id', 'agent_level', 'agent_name', 'agent_number', 'parent_id');
        }])->orderByRaw('if(agent_id=' . $user->agent_id . ',0,1),agent_id asc')->get();
        return self::jsonReturn($rs);
    }

    public function children_index($data = [])
    {
        $params = $data ?: request()->all();
        if (empty($params['where']['agent_id'])) {
            return self::jsonReturn();
        }
        $children_list = Agent::getAllChildrenAgents($params['where']['agent_id'], '', 1, true);
        if (!$children_list) return self::jsonReturn();
        $model = UAgentDayAchievementReport::whereIn('agent_id', $children_list)
            ->whereBetween('occur_time', $params['whereBetween']['occur_time'])
            ->groupBy('agent_id')
            ->select('agent_id');
        if (!empty($params['count'])) {
            $sql = $model->toSql();
            $rs['count'] = \DB::select('select count(*) as num from (' . $sql . ')a'
                , array_merge((array)$children_list, $params['whereBetween']['occur_time']))[0]->num;
        }
        if (!empty($params['limit'])) $model->limit($params['limit'])->offset($params['offset']);
        $rs['list'] = $model->selectRaw($this->select)->with(['agent' => function ($query) {
            $query->select('id', 'agent_level', 'agent_name', 'agent_number', 'parent_id');
        }])->orderByRaw('agent_id asc')->get();
        return self::jsonReturn($rs);
    }


    public function tree_index($data = [])
    {
        $params = $data ?: request()->all();
        $params['where']['agent_id'] = !empty($params['where']['agent_id']) ? $params['where']['agent_id'] : \Auth::user()->agent_id;
        $rs_self = UAgentDayAchievementReport::where('agent_id', $params['where']['agent_id'])
            ->whereBetween('occur_time', $params['whereBetween']['occur_time'])
            ->groupBy('agent_id')
            ->selectRaw('agent_id,
                sum(direct_cust_day_incharge) as total_incharge,
                sum(direct_cust_day_cash) as total_cash,
                sum(direct_cust_day_stock_finance) as total_stock_finance,
                sum(direct_cust_day_make_deal) as total_make_deal,
                sum(direct_cust_day_fee) as total_fee,
                sum(direct_cust_day_interests) as total_interests, 
                sum(direct_cust_day_interests+home_cust_day_interests_percentage) as total_interests_percentage,
                sum(direct_cust_day_commision) as total_commision,
                sum(direct_cust_day_commision+home_cust_day_commision_percentage) as total_commision_percentage,
                sum(direct_cust_adv_day_cost) as total_cost
            ')->with(['agent' => function ($query) {
                $query->select('id', 'agent_level', 'agent_name', 'agent_number', 'parent_id');
            }])->get();
        $children_list = Agent::getAllChildrenAgents($params['where']['agent_id'], '', 1, true);
        if (!$children_list) return self::jsonReturn();
        $model = UAgentDayAchievementReport::whereIn('agent_id', $children_list)
            ->whereBetween('occur_time', $params['whereBetween']['occur_time'])
            ->groupBy('agent_id')
            ->select('agent_id');
        if (!empty($params['count'])) {
            $sql = $model->toSql();
            $rs['count'] = \DB::select('select count(*) as num from (' . $sql . ')a'
                , array_merge((array)$children_list, $params['whereBetween']['occur_time']))[0]->num;
        }
        if (!empty($params['limit'])) {
            if ($params['offset'] == 0) $params['limit']--;
            $model->limit($params['limit'])->offset($params['offset']);
        }
        $rs_children = $model->selectRaw('
            sum(direct_cust_day_incharge+home_cust_day_incharge) as total_incharge,          
            sum(direct_cust_day_cash+home_cust_day_cash) as total_cash,            
            sum(direct_cust_day_stock_finance+home_cust_day_stock_finance) as total_stock_finance,            
            sum(direct_cust_day_make_deal+home_cust_dayl_make_deal) as total_make_deal,            
            sum(direct_cust_day_fee+home_cust_day_fee) as total_fee,            
            sum(direct_cust_day_interests+home_cust_day_interests+home_cust_day_interests_percentage) as total_interests,
            sum(direct_cust_day_interests+home_cust_day_interests_percentage) as total_interests_percentage,
            sum(direct_cust_day_commision+home_cust_day_commision+home_cust_day_commision_percentage) as total_commision,            
            sum(direct_cust_day_commision+home_cust_day_commision_percentage) as total_commision_percentage,
            sum(direct_cust_adv_day_cost+home_cust_adv_day_cost) as total_cost            
            ')->with(['agent' => function ($query) {
            $query->select('id', 'agent_level', 'agent_name', 'agent_number', 'parent_id');
        }])->orderByRaw('agent_id asc')->get();
        /*if(!$params['limit']){
            print_r($rs_self->toArray());
            print_r($rs_children->toArray());
        }*/

        $rs['list'] = array_merge($rs_self->toArray(), $rs_children->toArray());
        return self::jsonReturn($rs);
    }

}