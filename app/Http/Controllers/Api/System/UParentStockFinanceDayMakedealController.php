<?php

namespace App\Http\Controllers\Api\System;

use App\Http\Controllers\Controller;

class UParentStockFinanceDayMakedealController extends Controller
{
    use \App\Http\Controllers\Load\ShowTrait;

    public static $model_name = 'UParentStockFinanceDayMakedeal';

    public function __construct()
    {
        $this->middleware(["auth:api", "auth.request"]);
    }

}