<?php

namespace App\Http\Controllers\Api\System;

use App\Http\Controllers\Controller;

class UParentStockFinanceEntrustHistoryController extends Controller
{
    use \App\Http\Controllers\Load\ShowTrait;

    public static $model_name = 'UParentStockFinanceEntrustHistory';

    public function __construct()
    {
        $this->middleware(["auth:api", "auth.request"]);
    }
}