<?php

namespace App\Http\Controllers\Api\System;

use App\Http\Controllers\Controller;

class ParentStockFinanceController extends Controller
{
    use \App\Http\Controllers\Load\ShowTrait, \App\Http\Controllers\Load\UpdateTrait, \App\Http\Controllers\Load\StoreTrait;

    public static $model_name = 'ParentStockFinance';

    public function __construct()
    {
        $this->middleware(["auth:api", "auth.request"]);
    }

    public function update($id = '', $data = [])
    {
        $params = $data ?: request()->only(['financier_name', 'account', 'password',
            'communication_pw', 'total_in_capital', 'securities_trader', 'capital_id', 'account_status']);
        $rs = $this->doUpdate($id, $params);
        if ($rs) return self::jsonReturn([], 1, '更新成功！');
        return self::jsonReturn([], 0, '更新失败！');
    }

    public function store($data = [])
    {
        $params = $data ?: request()->only(['financier_name', 'account', 'password',
            'communication_pw', 'total_in_capital', 'securities_trader', 'capital_id', 'account_status']);
        $rs = $this->doStore($params);
        if ($rs) return self::jsonReturn([], 1, '新增成功！');
        return self::jsonReturn([], 0, '新增失败！');
    }
}