<?php

namespace App\Http\Controllers\Api\System;

use App\Http\Controllers\Controller;

class UParentStockFinanceDayMakedealHistoryController extends Controller
{
    use \App\Http\Controllers\Load\ShowTrait;

    public static $model_name = 'UParentStockFinanceDayMakedealHistory';

    public function __construct()
    {
        $this->middleware(["auth:api", "auth.request"]);
    }
}