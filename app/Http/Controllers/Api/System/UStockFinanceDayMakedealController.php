<?php

namespace App\Http\Controllers\Api\System;

use App\Http\Controllers\Controller;

class UStockFinanceDayMakedealController extends Controller
{
    use \App\Http\Controllers\Load\ShowTrait;

    public static $model_name = 'UStockFinanceDayMakedeal';

    public function __construct()
    {
        $this->middleware(["auth:api", "auth.request"]);
    }

    public function fee_list($data = [])
    {
        $params = $data ?: request()->all();
        $offset = $params['offset'];
        $limit = $params['limit'];
        unset($params['offset'], $params['limit'], $params['count']);
        $Model_1 = static::_filter_orm_permission($params, 'UStockFinanceDayMakedeal');
        $rs_1 = $Model_1->selectRaw('count(*) as count,sum(commission) as commission,sum(makedeal_amount) as makedeal_amount')->first();
        $Model_2 = static::_filter_orm_permission($params, 'UStockFinanceDayMakedealHistory');
        $rs_2 = $Model_2->selectRaw('count(*) as count,sum(commission) as commission,sum(makedeal_amount) as makedeal_amount')->first();
        $rs['count'] = $rs_1['count'] + $rs_2['count'];
        $rs['sum_commission'] = sprintf("%.3f", $rs_1['commission'] + $rs_2['commission']);
        $rs['sum_makedeal_amount'] = sprintf("%.3f", $rs_1['makedeal_amount'] + $rs_2['makedeal_amount']);
        $list = [];
        if ($offset < $rs_1['count']) {
            if ($offset + $limit <= $rs_1['count']) {
                $params['offset'] = $offset;
                $params['limit'] = $limit;
                $list = static::_run_orm($params, false, 'UStockFinanceDayMakedeal')['list'];
            } elseif ($offset + $limit > $rs_1['count'] && $offset < ($rs_1['count'] + $rs_2['count'])) {
                $params['offset'] = $offset;
                $params['limit'] = $rs_1['count'] - $offset;
                $Model_1 = static::_run_orm($params, false, 'UStockFinanceDayMakedeal');
                $params['offset'] = 0;
                $params['limit'] = $limit + $offset - $rs_1['count'];
                $Model_2 = static::_run_orm($params, false, 'UStockFinanceDayMakedealHistory');
                $list = array_merge($Model_1['list']->toArray(), $Model_2['list']->toArray());
            }
        } elseif ($offset >= $rs_1['count'] && $offset < ($rs_1['count'] + $rs_2['count'])) {
            $params['offset'] = $offset - $rs_1['count'];
            $params['limit'] = $limit;
            $list = static::_run_orm($params, false, 'UStockFinanceDayMakedealHistory')['list'];
        }
        $rs['list'] = $list;
        return self::jsonReturn($rs);
    }
}