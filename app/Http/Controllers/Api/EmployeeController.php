<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Model\AgentProfitRateConfig;
use App\Http\Model\ClientFeeRate;
use App\Http\Model\Employee;
use App\Http\Model\EmployeeProfitRateConfig;
use App\Http\Model\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

/**
 * Class EmployeeController 代理商员工控制器
 * @package App\Http\Controllers\Api
 */
class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:api")->except(['search']);
    }

    /*
     * 创建代理商员工
     */
    public function create(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|unique:a_agent_emp',//验证登陆用户名唯一
            'email' => 'unique:a_agent_emp',//验证登陆用户名唯一
            'phone' => 'required|unique:a_agent_emp',
            'employee_name' => 'required',
            'is_forbid' => ['required', Rule::in(['0', '1'])],
        ], [
            'phone.unique' => "联系人手机号码已注册",
            'name.unique' => "登陆用户名不能重复",
            'employee_name' => '员工真实姓名不能为空',
        ]);
        $agent_id = Auth::user()->agent_id;

        $msg = $this->validateParentAgentPercentageInput($request, $agent_id);
        if ($msg) {
            return $this::jsonReturn([], 0, $msg . '请重新设定分成比例');
        }
        if ($validator->fails()) {
            return parent::jsonReturn([], parent::CODE_FAIL, $validator->errors()->first());
        }

        DB::beginTransaction();
        try {
            $agent_id = Auth::user()->agent_id;

            $employeeData = $request->only('employee_name', 'phone', 'is_forbid', 'remark', 'name', 'role_id', 'email');
            if (!$request->email) {
                $employeeData['email'] = $request->phone . '@yingli.com';
            }

            $employeeData['password'] = bcrypt($request->input('password'));
            $employeeData['agent_id'] = $agent_id;
            $employ = Employee::firstOrCreate($employeeData);

            //创建配置
            $rateWhere = [
                'employee_id' => $employ->id,
            ];
            //天配

            $rateWhere['type'] = EmployeeProfitRateConfig::TypeDay;
            $percentage = $request->input('day_percentage');
            EmployeeProfitRateConfig::updateOrCreate($rateWhere, compact('percentage'));
            //月配
            $rateWhere['type'] = EmployeeProfitRateConfig::TypeMonth;
            $percentage = $request->input('month_percentage');
            EmployeeProfitRateConfig::updateOrCreate($rateWhere, compact('percentage'));
            //佣金oen
            $rateWhere['type'] = EmployeeProfitRateConfig::TypeCommissionOne;
            $percentage = $request->input('commission_percentage');
            EmployeeProfitRateConfig::updateOrCreate($rateWhere, compact('percentage'));

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return self::jsonReturn([], self::CODE_FAIL, $e->getMessage());
        }

        return self::jsonReturn([], self::CODE_SUCCESS, '创建代理商员工成功');


    }

    /*
     * 更新代理商员工信息
     */
    public function update(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            //'name' => 'required|unique:s_system_user',//验证登陆用户名唯一
            'employee_name' => 'required',
            'id' => 'required',
            'is_forbid' => [
                'required',
                Rule::in(['0', '1']),
            ],
        ], [
            'name.unique' => "登陆用户名不能重复",
            'email.unique' => "登陆用户名不能重复",
            'employee_name' => '员工真实姓名不能为空',
        ]);
        $agent_id = $request->agent_id;
        $msg = $this->validateParentAgentPercentageInput($request, $agent_id);
        if ($msg) {
            return $this::jsonReturn([], 0, $msg . '请重新设定分成比例');
        }
        if ($validator->fails()) {
            return parent::jsonReturn([], parent::CODE_FAIL, $validator->errors()->first());
        }


        DB::beginTransaction();
        try {

            $employeeData = $request->only('employee_name', 'phone', 'is_forbid', 'remark', 'role_id', 'email');

            if (strlen($request->password) > 6) {
                $employeeData['password'] = bcrypt($request->input('password'));
            }
            Employee::where($request->only('id'))->update($employeeData);


            //创建配置
            $rateWhere = [
                'employee_id' => $request->id,
            ];
            //天配

            $rateWhere['type'] = EmployeeProfitRateConfig::TypeDay;
            $percentage = $request->input('day_percentage');
            EmployeeProfitRateConfig::updateOrCreate($rateWhere, compact('percentage'));
            //月配
            $rateWhere['type'] = EmployeeProfitRateConfig::TypeMonth;
            $percentage = $request->input('month_percentage');
            EmployeeProfitRateConfig::updateOrCreate($rateWhere, compact('percentage'));
            //佣金oen
            $rateWhere['type'] = EmployeeProfitRateConfig::TypeCommissionOne;
            $percentage = $request->input('commission_percentage');
            EmployeeProfitRateConfig::updateOrCreate($rateWhere, compact('percentage'));


            foreach ([
                         0 => $request->day_percentage,
                         1 => $request->month_percentage,
                         2 => $request->commission_percentage
                     ] as $type => $vo) {
                ClientFeeRate::where(['emp_id' => $request->id, 'type' => $type])->update(['emp_rate' => $vo]);
            }

            DB::commit();
            return self::jsonReturn([], self::CODE_SUCCESS, '更新代理商员工成功');
        } catch (\Exception $e) {
            DB::rollBack();
            return self::jsonReturn([], self::CODE_FAIL, $e->getMessage());
        }

    }


    /**
     * 代理商的信息信息 配置 附加信息
     * @return string
     */
    public function info(Request $request)
    {
        $employ = Employee::with('percentages')->find($request->employee_id)->toArray();
        $employ['roleOptions'] = Role::all();
        return self::jsonReturn($employ);
    }


    /**
     * 代理商员工表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request)
    {
        $user = Auth::user();

        $query = Employee::orderByDesc('updated_time')->where('role_id', Role::ROLE_STAFF_AGENT)->with('agent', 'role',
            'percentages');
        //如果不是系统管理员 而且 而且有代理商ID
        //只显示自己的员工

        $agent_id = $request->input('agent_id');
        if ($user->getIsSystemAdminAttribute()) {
            if ($agent_id) {
                $query->where(compact('agent_id'));
            } else {
                //显示全部
            }
        } else {
            //不是系统管理员就显示自己的客户
            $query->where('agent_id', $user->agent_id);
        }

        $keyword = $request->input('keyword');
        if ($keyword) {
            $query->where(function ($subQ) use ($keyword) {
                $subQ->orWhere('employee_name', 'like', "%$keyword%")
                    ->orWhere('phone', 'like', "%$keyword%");
            });
        }
        $list = $query->paginate($request->input('size', self::PAGE_SIZE));

        return self::jsonReturn($list);
    }


    /**
     * 验证父级代理商的分成比例  给员工的比例不能超过自己的分成比例
     */
    protected function validateParentAgentPercentageInput(Request $request, $agent_id)
    {
        $typeNames = ['天配', '月配', '佣金'];
        $inputNames = ['day_percentage', 'month_percentage', 'commission_percentage'];

        $msg = null;
        foreach ($typeNames as $key => $name) {
            $p = AgentProfitRateConfig::where(['agent_id' => $agent_id, 'type' => $key])->first();

            if (!$p) {
                return $this::jsonReturn([], 0, '所属代理商分配比例不存在,数据不完整');
                exit(200);
            }

            $maxP = $p->percentage;
            $percentage = $request->input($inputNames[$key]);
            if ($percentage >= $maxP) {
                $msg .= "父代理商-最大{$name}:{$maxP}\r\n";
            }
        }
        return $msg;

    }


}