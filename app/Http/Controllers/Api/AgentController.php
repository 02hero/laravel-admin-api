<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Model\Agent;
use App\Http\Model\AgentInfo;
use App\Http\Model\AgentProfitRateConfig;
use App\Http\Model\CapitalPool;
use App\Http\Model\ClientFeeRate;
use App\Http\Model\Employee;
use App\Http\Model\EmployeeProfitRateConfig;
use App\Http\Model\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * Class AgentController 代理商
 * @package App\Http\Controllers\Api
 */
class AgentController extends Controller
{

    const AgentSelectorListCackeKey = 'agent.selector.list.cache.key';

    public function __construct()
    {
        $this->middleware("auth:api")->except(['search']);
    }

    /*
     * 创建代理商
     */
    public function createAgent(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            //'bank_account' => 'required|unique:a_agent',
            'agent_name' => 'required|unique:a_agent',
            'agent_number' => 'required|unique:a_agent',
            'agent_number' => 'unique:a_agent_emp,name',
            //'bank_account' => 'required|unique:a_agent',
            'owner_name' => 'required|unique:a_agent',
            'phone' => 'required|unique:a_agent',
            //'phone' => 'required|unique:a_agent_emp',
            'name' => 'required|unique:a_agent_emp',//验证登陆用户名唯一
        ], [
            'agent_name.unique' => "代理商名称不能重复",
            //'bank_account.unique' => "提现银行卡号重复",
            'phone.unique' => "联系人手机号码已注册",
            'name.unique' => "登陆用户名以重复",
            'owner_name.unique' => "代理商联系人姓名重复",

        ]);

        $msg = $this->validateParentAgentPercentageInput($request);
        if ($msg) {
            return $this::jsonReturn([], 0, $msg . '超过父级:请重新设定分成比例');
        }


        if ($validator->fails()) {
            return parent::jsonReturn([], parent::CODE_FAIL, $validator->errors()->first());
        }

        Cache::forget(self::AgentSelectorListCackeKey);

        $msg = '';
        DB::beginTransaction();
        try {

            //$user = Auth::user();
            //确定确定权限
            $parent_id = $request->input('parent_id');
            $parent_agent = Agent::find($parent_id);
            if ($parent_agent) {
                $agent_level = $parent_agent->agent_level + 1;
            } else {
                $agent_level = 1;
            }

            $data = $request->except([
                'password',
                'confirm_password',
                'day_percentage',
                'month_percentage',
                'commission_percentage',
                'name'
            ]);
            $data['agent_level'] = $agent_level;
            $data['agent_number'] = $request->input('name');
            //创建代理商
            $instance = Agent::create($data);
            //创建登陆用户
            $agent_id = $instance->id;


            //创建配置
            //天配
            $rateWhere = [
                'agent_id' => $agent_id,
                'type' => AgentProfitRateConfig::TypeDay,
            ];
            $percentage = $request->input('day_percentage');
            AgentProfitRateConfig::updateOrCreate($rateWhere, compact('percentage'));
            //月配
            $rateWhere = [
                'agent_id' => $agent_id,
                'type' => AgentProfitRateConfig::TypeMonth,
            ];
            $percentage = $request->input('month_percentage');
            AgentProfitRateConfig::updateOrCreate($rateWhere, compact('percentage'));
            //佣金oen
            $rateWhere = [
                'agent_id' => $agent_id,
                'type' => AgentProfitRateConfig::TypeCommissionOne
            ];
            $percentage = $request->input('commission_percentage');
            AgentProfitRateConfig::updateOrCreate($rateWhere, compact('percentage'));

            //创建登陆账号
            $hashedPassword = bcrypt($request->input('password'));
            $phone = $request->input('phone');
            $name = $request->input('name');
            $userWhere = compact('phone', 'agent_id');
            $userData = [
                'password' => $hashedPassword,
                'phone' => $phone,
                'role_id' => Role::ROLE_ADMIN_AGENT,
                'name' => $name,
                'employee_name' => $request->owner_name
            ];

            Employee::updateOrCreate($userWhere, $userData);


            //生成推广码  不需要
            //查询的时候 如果推荐码不存在 就创建一条记录
            //这个逻辑在添加的时候交个了查询的时候

            DB::commit();
            return self::jsonReturn($instance, self::CODE_SUCCESS, '代理商创建成功');
        } catch (\Exception $e) {
            $msg .= $e->getMessage();
            DB::rollBack();
            return self::jsonReturn([], self::CODE_FAIL, $msg);
        }


    }

    /**
     * 下拉列表搜索代理商
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function selectorOptionsList(Request $request)
    {
        if (Auth::user()->isSystemAdmin) {
            $list = Agent::where('is_locked', '!=', 1)->where('agent_level', '<', 5)
                ->orderByDesc('updated_time')->select('id', 'agent_level', 'agent_name', 'capital_id')
                ->get();
        } else {
            $thisAgent = Auth::user()->agent;
            $list = Agent::getAllChildrenAgentWithMyself($thisAgent);
        }


//        $list = Cache::rememberForever(self::AgentSelectorListCackeKey, function () {
//            return Agent::where('is_locked', '!=', 1)
//                ->orderByDesc('updated_time')->select('id', 'agent_level', 'agent_name')
//                ->get();
//
//        });
        return self::jsonReturn($list);
    }

    /**
     * 代理商的信息信息 配置 附加信息
     * @return string
     */
    public function info(Request $request)
    {
        //代理商基本信息
        $agent_id = $request->input('id');
        $basic = Agent::find($agent_id);
        //代理商配置信息s
        $configs = AgentProfitRateConfig::where(compact('agent_id'))->get();

        //代理商附加信息
        $info = AgentInfo::firstOrNew(['id' => $agent_id]);
        //代理商管理员
        $user = User::where(['agent_id' => $agent_id, 'role_id' => Role::ROLE_ADMIN_AGENT])->first();

        $parent = Agent::find($basic->parent_id);

        return self::jsonReturn(compact('basic', 'info', 'configs', 'user', 'parent'));
    }


    /**
     * 代理商列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request)
    {
        $per_page = $request->input('size', self::PAGE_SIZE);
        $keyword = $request->input('keyword');
        $user = Auth::user();
        $agentIdsArray = Agent::getAllChildrenAgents($user->agent_id);
        //todo:判断权限 增加预设条件
        $query = Agent::orderBy('is_locked', 'asc')
            ->orderBy('created_time', 'desc')
            ->whereIn('id', $agentIdsArray)
            ->with('parent', 'info', 'percentages');
        if ($keyword) {
            $query->orWhere('agent_name', 'like', "%$keyword%")
                ->orWhere('agent_number', 'like', "$keyword")
                ->orWhere('id', 'like', "%$keyword%");
        }
        $data = $query->paginate($per_page);
        return self::jsonReturn($data);
    }


    /**
     * 代理商只能显示自己的下级
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function childrenAgent(Request $request)
    {
        $thisAgentId = Auth::user()->agent_id;
        $agentID = $request->input('agent_id', $thisAgentId);
        $thisAgent = Agent::with('children')->with([
            'children' => function ($query) {
                $query->with('parent', 'percentages', 'info');
            }
        ])->find($agentID);
        if ($thisAgent->children) {
//            $list = Cache::remember(self::generateCacheKeyByReqeust(), 1, function () use ($thisAgent) {
//                return $this->getAllChildrenAgentWithMyselfWithInfoParentPercentage($thisAgent);
//            });
            return self::jsonReturn($thisAgent->children, self::CODE_SUCCESS);
        } else {
            return self::jsonReturn([], self::CODE_FAIL, '找不到代理商,无效ID');
        }
    }

    /**
     * 修改代理商的管理员密码
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeAgentAdminUserPassword(Request $request)
    {
        $password = $request->input('password');
        $confirm_password = $request->input('confirm_password');
        $agent_id = $request->input('agent_id');
        $role_id = $request->input('role_id');
        $id = $request->input('id');


        if ($password != $confirm_password) {
            return self::jsonReturn([], self::CODE_FAIL, '两次输入密码不一样');
        }

        try {
            $pp = bcrypt($password);
            User::where(compact('id', 'agent_id', 'role_id'))->update(['password' => $pp]);
            return self::jsonReturn([], self::CODE_SUCCESS, '修改密码成功');

        } catch (\Exception $e) {
            $message = $e->getMessage();
            return self::jsonReturn([], self::CODE_FAIL, $message);
        }


    }

    /**
     * 修改a_agent信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAgentBasic(Request $request)
    {
        Cache::forget(self::AgentSelectorListCackeKey);
        $id = $request->id;
        $validator = \Validator::make($request->all(), [
            'bank_account' => 'required|unique:a_agent,id,' . $id,
            'agent_name' => 'required|unique:a_agent,id,' . $id,
            'agent_number' => 'required|unique:a_agent,id,' . $id,
            'owner_name' => 'required|unique:a_agent,id,' . $id,
            'phone' => 'required|unique:a_agent,id,' . $id,
        ], [
            'agent_number.unique' => "代理商编号不能重复",
            'agent_name.unique' => "代理商名称不能重复",
            'bank_account.unique' => "提现银行卡号重复",
            'phone.unique' => "联系人手机号码已注册",
            'owner_name.unique' => "代理商联系人姓名重复",
        ]);
        if ($validator->fails()) {
            return parent::jsonReturn([], parent::CODE_FAIL, $validator->errors()->first());
        }

        try {
            $agent = Agent::find($request->id)->fill($request->except('id'));
            $code = $agent->save();
            return self::jsonReturn($agent, $code, '修改代理商基本信息成功');

        } catch (\Exception $eee) {
            return parent::jsonReturn([], parent::CODE_FAIL, $eee->getMessage());

        }
    }

    /**
     * 修改a_agent_extra_info 表信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAgentInfo(Request $request)
    {
        $agentInfo = AgentInfo::updateOrCreate($request->only('id'), $request->all());

        $guzzleClient = new \GuzzleHttp\Client(['http_errors' => false]);
        //产生微信菜单
        $response = $guzzleClient->request('POST', config('app.generateWechatMenu'), [
            'form_params' => ['agentId' => $request->id]
        ]);

        if ($response->getStatusCode() !== 200) {
            return self::jsonReturn($agentInfo, self::CODE_FAIL, '生成微信菜单:无效公众号配置');
        }
        $resObj = \GuzzleHttp\json_decode($response->getBody());
        if ($resObj->status !== 1) {
            return self::jsonReturn($agentInfo, self::CODE_FAIL, '代理商创建微信菜单失败,请检查微信公众号相关配置');
        }
        //修改客户微信二维码
        $response = $guzzleClient->request('POST', config('app.changeCustomerWechatQrcode'), [
            'form_params' => ['agentId' => $request->id, 'isAllClient' => 1]
        ]);
        if ($response->getStatusCode() !== 200) {
            return self::jsonReturn($agentInfo, self::CODE_FAIL, '生成微信菜单:无效公众号配置');
        }
        $resObj = \GuzzleHttp\json_decode($response->getBody());
        if ($resObj->status !== 1) {
            return self::jsonReturn($agentInfo, self::CODE_FAIL, '更换代理客户二维码失败,请检查微信公众号相关配置');
        }

        return self::jsonReturn($agentInfo, self::CODE_SUCCESS, '修改代理商附加信息信息成功');

    }

    /**
     * 修改 a_agent_percentage_setting 修改代理商分成表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAgentPercentage(Request $request)
    {
        $agent_id = $request->input('agent_id');

        $theAgent = Agent::find($agent_id);
        $agent_level = $theAgent->agent_level;
        $agentNameKey = "agent{$agent_level}";
        $agentPercentageKey = "agent{$agent_level}_rate";


        $msg = $this->validateParentAgentPercentageInput($request);
        if ($msg) {
            return $this::jsonReturn([], 0, $msg . '超过父级:请重新设定分成比例');
        }
        $msg = $this->validateChildrenAgentPercentageInput($request);
        if ($msg) {
            return $this::jsonReturn([], 0, $msg . '超过子级代理:请重新设定分成比例');
        }
        $msg = $this->validateChildrenStaffPercentageInput($request);
        if ($msg) {
            return $this::jsonReturn([], 0, $msg . '超过员工:请重新设定分成比例');
        }


        try {
            //todo::修改归属关系表
            $inputNames = ['day_percentage', 'month_percentage', 'commission_percentage'];
            $idNames = ['day_id', 'month_id', 'commission_id'];
            foreach ($inputNames as $type => $name) {
                $percentage = $request->input($name);
                $id = $request->input($idNames[$type]);
                AgentProfitRateConfig::updateOrInsert(compact('agent_id', 'id', 'type'), compact('percentage'));
                ClientFeeRate::where([
                    $agentNameKey => $agent_id,
                    'type' => $type
                ])->update([$agentPercentageKey => $percentage]);
            }

            return self::jsonReturn([], self::CODE_SUCCESS, '修改代理商分成配置成功');
        } catch (\Exception $eee) {
            return parent::jsonReturn([], parent::CODE_FAIL, $eee->getMessage());

        }
    }


    private function getAllChildrenAgentWithMyselfWithInfoParentPercentage(Agent $agent)
    {
        $collections = collect([$agent]);

        $parentIds = [$agent->id];
        $i = 0;
        while (count($parentIds)) {
            $childrens = Agent::whereIn('parent_id', $parentIds)->with('parent', 'percentages', 'info')->get();
            if (count($childrens)) {
                $collections = $collections->merge($childrens);
                $parentIds = $childrens->pluck('id')->all();
                $parentIds = array_values($parentIds);
            } else {
                $parentIds = null;
            }
            //双保险
            if ($i > 4) {
                $parentIds = null;
            }
            $i++;
        }
        return $collections;
    }


    /**
     * 验证父级代理商的分成比例
     */
    protected function validateParentAgentPercentageInput(Request $request)
    {
        $parent_id = $request->parent_id;
        $typeNames = ['天配', '月配', '佣金'];
        $inputNames = ['day_percentage', 'month_percentage', 'commission_percentage'];

        $msg = null;
        foreach ($typeNames as $key => $name) {
            $p = AgentProfitRateConfig::where(['agent_id' => $parent_id, 'type' => $key])->first();
            if (empty($p)) {
                $msg .= '父级代理商分成数据不完成或者不存在';
                return $msg;
            }
            $maxP = $p->percentage;
            $percentage = $request->input($inputNames[$key]);
            if ($percentage >= $maxP) {
                //不通过
                $msg .= "父级代理商-最大{$name}:{$maxP}\r\n";
            }
            //检验下级代理商分成配置

        }
        return $msg;

    }

    /**
     * 检验下级代理商分成比例  创建的时候不需要检测  只有更新的时候需要检测
     */
    protected function validateChildrenAgentPercentageInput(Request $request)
    {
        $msg = null;
        $this_agent_id = $request->agent_id;//更新的时候有这参数
        $childrenAgentIds = Agent::where('parent_id', $this_agent_id)->pluck('id')->toArray();
        if (!$childrenAgentIds) {
            //没有下级代理商就不用在验证了
            return $msg;
        }
        $childrenAgentIds = array_values($childrenAgentIds);

        $typeNames = ['天配', '月配', '佣金'];
        $inputNames = ['day_percentage', 'month_percentage', 'commission_percentage'];

        foreach ($typeNames as $key => $name) {
            $maxP = AgentProfitRateConfig::where('type', $key)->whereIn('agent_id',
                $childrenAgentIds)->max('percentage');
            $percentage = $request->input($inputNames[$key]);
            if ($percentage < $maxP) {
                //不通过
                $msg .= "后代代理商-最大{$name}:{$maxP}\r\n";
            }
            //检验下级代理商分成配置

        }
        return $msg;

    }

    /**
     * 检验下级员工分成比例 创建的时候不需要检测  只有更新的时候需要检测
     */
    protected function validateChildrenStaffPercentageInput(Request $request)
    {
        $msg = null;
        $this_agent_id = $request->agent_id;//更新的时候有这参数
        $mySlaveIds = Employee::where('agent_id', $this_agent_id)->where('role_id',
            Role::ROLE_STAFF_AGENT)->pluck('id')->toArray();
        if (!$mySlaveIds) {
            //没有下级代理商就不用在验证了
            return $msg;
        }
        $mySlaveIds = array_values($mySlaveIds);

        $typeNames = ['天配', '月配', '手续费'];
        $inputNames = ['day_percentage', 'month_percentage', 'commission_percentage'];

        foreach ($typeNames as $key => $name) {
            $maxP = EmployeeProfitRateConfig::where('type', $key)->whereIn('employee_id',
                $mySlaveIds)->max('percentage');
            $percentage = $request->input($inputNames[$key]);
            if ($percentage < $maxP) {
                //不通过
                $msg .= "后代员工-最大{$name}:{$maxP}\r\n";
            }
            //检验下级代理商分成配置
        }
        return $msg;
    }


    /**
     * 资金池信息
     */

    public function agentCapitalPoolListWithInfo()
    {
        $parent_agent_capital_id = Auth::user()->agent->capital_id;
        $list = CapitalPool::all();
        return self::jsonReturn(compact('list', 'parent_agent_capital_id'));
    }

}