<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Model\ContentHelp;
use App\Http\Model\Role;
use Illuminate\Http\Request;

class ContentHelpController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:api")->except(['search']);
    }

    /**
     * 帮助列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function helps(Request $request)
    {
        $user = $request->user();
        $per_page = $request->input('size', self::PAGE_SIZE);
        $keyword = $request->input('keyword');
        $query = ContentHelp::with("type")->orderBy('created_time', 'desc');
        if ($keyword) {
            $query->orWhere('id', 'like', "%$keyword%")
                ->orWhere("agent_id", "like", "%$keyword%");
        }

        //代理商
        if ($user->role_id != Role::ROLE_ADMIN_SYSTEM) {
            $query->where("agent_id", $user->agent_id);
        }

        $data = $query->paginate($per_page);
        return self::jsonReturn($data);
    }

    /**
     * 获取帮助信息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHelpInfo(Request $request)
    {
        $user = \Auth::user();
        $query = ContentHelp::where('id', $request->get("id"));
        //代理商
        if ($user->role_id != Role::ROLE_ADMIN_SYSTEM) {
            $query->where("agent_id", $user->agent_id);
        }
        $ret = $query->first();
        return  self::jsonReturn($ret);
    }

    /**
     * 编辑帮助
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function helpEdit(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'help_type' => 'required|integer|min: 1',
        ], [
            "帮助类别错误",
        ]);

        if ($validator->fails()) {
            return parent::jsonReturn([], parent::CODE_FAIL, $validator->errors()->first());
        }

        $user = \Auth::user();
        $data = $request->only(["content", "help_type"]);
        foreach ($data as $k=>$v) {
            if ($v === null) $data[$k] = "";
        }

        $id = $request->get("id");
        if ($id) {
            $helpQuery = ContentHelp::where("id", $id);
            if ($user->role_id != Role::ROLE_ADMIN_SYSTEM) {
                $helpQuery->where("agent_id", $user->agent_id);
            }
            $help = $helpQuery->first();
            if (!$help) return self::jsonReturn([], self::CODE_FAIL, "信息错误");

            $ret = $help->update($data);
        } else {
            $data = array_merge($data, [
                "agent_id"=>$user->agent_id,
                "employee_id"=>$user->id,
            ]);

            $ret = ContentHelp::create($data);
        }

        return $ret ? self::jsonReturn([], self::CODE_SUCCESS, "操作成功") :
            self::jsonReturn([], self::CODE_FAIL, "操作失败");
    }

    /**
     * 删除帮助
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function helpDelete(Request $request)
    {
        $user = \Auth::user();
        $query = ContentHelp::where('id', $request->get("id"));
        //代理商
        if ($user->role_id != Role::ROLE_ADMIN_SYSTEM) {
            $query->where("agent_id", $user->agent_id);
        }
        $helpType = $query->first();
        if (!$helpType) return self::jsonReturn([], self::CODE_FAIL, "该帮助不存在");

        $ret = $helpType->delete();
        return self::jsonReturn([]);
    }

}