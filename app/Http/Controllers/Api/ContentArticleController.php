<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Model\ContentArticle;
use App\Http\Model\Role;
use Illuminate\Http\Request;

class ContentArticleController extends Controller
{
    private $articleTypes = [
        ["id"=>1, "name"=>"股票配资新闻"],
        ["id"=>2, "name"=>"配资常见问题"],
        ["id"=>3, "name"=>"公告"],
    ];

    public function __construct()
    {
        $this->middleware("auth:api")->except(['search']);
    }

    /**
     * 文章列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function articles(Request $request)
    {
        $user = $request->user();
        $per_page = $request->input('size', self::PAGE_SIZE);
        $keyword = $request->input('keyword');
        $query = ContentArticle::orderBy('created_time', 'desc');
        if ($keyword) {
            $query->orWhere('title', 'like', "%$keyword%")
                ->orWhere('id', 'like', "%$keyword%")
                ->orWhere("agent_id", "like", "%$keyword%");
        }

        //代理商
        if ($user->role_id != Role::ROLE_ADMIN_SYSTEM) {
            $query->where("agent_id", $user->agent_id);
        }

        $data = $query->paginate($per_page);
        return self::jsonReturn($data);
    }

    /**
     * 获取文章类型
     * @return \Illuminate\Http\JsonResponse
     */
    public function articleTypes()
    {
        return self::jsonReturn($this->articleTypes);
    }

    /**
     * 获取文章详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getArticleInfo(Request $request)
    {
        $user = \Auth::user();
        $query = ContentArticle::where('id', $request->get("id"));
        //代理商
        if ($user->role_id != Role::ROLE_ADMIN_SYSTEM) {
            $query->where("agent_id", $user->agent_id);
        }
        $ret = $query->first();
        return  self::jsonReturn($ret);
    }

    /**
     * 编辑文章
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function articleEdit(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'type' => 'required|integer|between: 1, 3',
            "title" => "required|min:1",
        ], [
            "type.required"=>"文章类别错误",
            "type.integer"=>"文章类别错误",
            "type.between"=>"文章类别错误",
            "title.required" => "文章标题不能为空",
            "title.min" => "文章标题不能为空",
        ]);

        if ($validator->fails()) {
            return parent::jsonReturn([], parent::CODE_FAIL, $validator->errors()->first());
        }

        $user = \Auth::user();
        $data = array_merge($request->only(["title", "content", "type", "summary", "cover_pic", "sort"]),
            ["type_name"=>$this->getArticleTypeName($request->get("type"))]);
        foreach ($data as $k=>$v) {
            if ($v === null) $data[$k] = "";
        }

        $id = $request->get("id");
        if ($id) {
            $articleQuery = ContentArticle::where("id", $id);
            if ($user->role_id != Role::ROLE_ADMIN_SYSTEM) {
                $articleQuery->where("agent_id", $user->agent_id);
            }
            $article = $articleQuery->first();
            if (!$article) return self::jsonReturn([], self::CODE_FAIL, "信息错误");

            $ret = $article->update($data);
        } else {
            $data = array_merge($data, [
                "agent_id"=>$user->agent_id,
                "employee_id"=>$user->id,
            ]);

            $ret = ContentArticle::create($data);
        }

        return $ret ? self::jsonReturn([], self::CODE_SUCCESS, "操作成功") :
            self::jsonReturn([], self::CODE_FAIL, "操作失败");
    }

    /**
     * 删除文章
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function articleDelete(Request $request)
    {
        $user = \Auth::user();
        $query = ContentArticle::where('id', $request->get("id"));
        //代理商
        if ($user->role_id != Role::ROLE_ADMIN_SYSTEM) {
            $query->where("agent_id", $user->agent_id);
        }
        $helpType = $query->first();
        if (!$helpType) return self::jsonReturn([], self::CODE_FAIL, "该帮助不存在");

        $ret = $helpType->delete();
        return self::jsonReturn([]);
    }


    private function getArticleTypeName($type)
    {
        foreach ($this->articleTypes as $v) {
            if ($v["id"] == $type) return $v["name"];
        }

        return null;
    }
}