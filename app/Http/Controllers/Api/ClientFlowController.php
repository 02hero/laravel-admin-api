<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\System\JavaApiController;
use App\Http\Controllers\Controller;
use App\Http\Model\Client;
use App\Http\Model\ClientAgentEmployeeRelation;
use App\Http\Model\ClientFLow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class ClientFlowController 客户账户流水
 * @package App\Http\Controllers\Api
 */
class ClientFlowController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:api");
    }

    /**
     * 客户资金
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request)
    {
        $keyword = $request->keyword;
        $page_size = $request->input('size', self::PAGE_SIZE);

        $clientIds = Auth::user()->getAllMyClientIdArrayAttribute();


        $query = ClientFLow::orderByDesc('created_time')->with('client')->whereIn('cust_id', $clientIds);

        if ($keyword) {
            $likeString = "%$keyword%";
            $client_ids = Client::orWhere('id', 'like', $likeString)
                ->orWhere('real_name', 'like', $likeString)
                ->orWhere('cellphone', 'like', $likeString)
                ->pluck('id')->all();
            $query->whereIn('cust_id', array_values($client_ids));
        }
        $list = $query->paginate($page_size);
        return self::jsonReturn($list);
    }


    //手动给客户调整余额
    public function clientAcountFlowAdjust(Request $request)
    {
        $user = Auth::user();

        //权限控制
        if (!$user->getIsSystemAdminAttribute()) {
            return self::jsonReturn([], 0, '只有系统管理员才能够调整余额!!!');
        }

        $validator = \Validator::make($request->all(), [
            'cust_id' => 'exists:u_customer,id',
        ], [
            'cust_id.exists' => "要修改的客户不存在",
        ]);
        if ($validator->fails()) {
            return parent::jsonReturn([], parent::CODE_FAIL, $validator->errors()->first());
        }

        DB::beginTransaction();
        try {
            $flow_type = ClientFLow::AdminRechargeMoney;

            $cust_id = $request->cust_id;
            $amount_of_account = $request->money - $request->fee;

            $last_money = Client::find($cust_id)->cust_capital_amount;
            $account_left = $last_money + $amount_of_account;
            $operator_id = $user->id;
            $remark = "{$request->description}\r\n上期累计余额:{$last_money};修改金额:{$request->money}元;手续费:{$request->fee}元;有效金额:{$amount_of_account}元;本次累计:{$account_left}元\r\n";
            $remark .= "操作者:{$user->real_name},ID:{$user->id},手机号码:{$user->phone};后台登陆账号:{$user->name}\r\n";
            $remark .= "备注:{$request->remark}";
            $description = $request->description;


            $data = compact('cust_id', 'operator_id', 'flow_type', 'description', 'amount_of_account', 'account_left', 'remark');
            $relationData = ClientAgentEmployeeRelation::where(compact('cust_id'))->first()->toArray();
            //插入代理商层级关系
            $relationArray = array_only($relationData, ['agent1', 'agent2', 'agent3', 'agent4', 'agent5']);
            $relationArray['emp_agent_id'] = $relationData['belong_to_agent'];
            $relationArray['emp_id'] = $relationData['direct_emp_id'];

            $data = array_merge($data, $relationArray);
            ClientFLow::create($data);

            //修改客户资金可用余额
            Client::where('id', $cust_id)->increment('cust_capital_amount', $amount_of_account);
            //条用java接口来扣减冲抵帐
            (new JavaApiController())->StockFinanceRepay($cust_id, $amount_of_account);
            DB::commit();
            return self::jsonReturn([], self::CODE_SUCCESS, '添加客户流水成功');
        } catch (\Exception $e) {
            return self::jsonReturn([], 0, $e->getMessage());
        }
    }

}