<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Model\UAgentDayAchievementReport;
use Illuminate\Http\Request;

/**
 * 代理商当日绩表
 * Class UAgentDayAchievementReportController
 * @package App\Http\Controllers\Api
 */
class UAgentDayAchievementReportController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth:api")->except(['search']);
    }

    /**
     * 代理商业绩list
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request)
    {
        $query = UAgentDayAchievementReport::orderBy('agent_id', 'asc');

        $keyword = $request->input('keyword');
        if ($keyword) {
            $query->orWhere('agent_number', 'like', "%$keyword%")
                ->orWhere('agent_phone', 'like', "%$keyword%");
        }
        return self::jsonPaginationReturn($query);
    }


}
