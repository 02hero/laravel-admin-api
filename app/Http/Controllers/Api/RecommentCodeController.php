<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Model\Agent;
use App\Http\Model\AgentInfo;
use App\Http\Model\RecommendCode;
use App\Http\Model\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class RecommentCodeController  代理商和员工推荐码
 * @package App\Http\Controllers\Api
 */
class RecommentCodeController extends Controller
{


    public function __construct()
    {
        $this->middleware("auth:api")->except(['search']);
    }


    public function info(Request $request)
    {

        $user = Auth::user();
        //确定当前用户的角色
        if ($user->role_id == Role::ROLE_ADMIN_AGENT) {
            //这个用户可以直接代表代理商
            //他的推荐码就是代理商推荐码

            $condition = [
                'user_type' => RecommendCode::TYPE_AGENT,
                'user_id' => $user->agent_id,
            ];
            $recommendCodeInstance = RecommendCode::where($condition)->first();
        } else {
            if ($user->role_id == Role::ROLE_STAFF_AGENT) {
                //这个用户是代理杀过的员工他的
                //推荐码是员工推荐码

                $condition = [
                    'user_type' => RecommendCode::TYPE_EMPLOYEE,
                    'user_id' => $user->id,
                ];
                $recommendCodeInstance = RecommendCode::where($condition)->first();
            } else {
                return self::jsonReturn([], 0, '你没有权限产生二维码');
            }
        }
        //如没有推荐码记录就为他们创建一条记录
        if (!$recommendCodeInstance) {
            //如果没有推荐码就给他创建爱推荐码
            //写入数据库
            $rec_code = rand(100000, 999999);
            //找到不重复的rec_code
            $flag = true;
            while ($flag) {
                $one = RecommendCode::where(compact('rec_code'))->first();
                if ($one) {
                    $flag = true;
                    $rec_code = rand(100000, 999999);
                } else {
                    $flag = false;
                }
            }
            $condition['rec_code'] = $rec_code;
            $recommendCodeInstance = RecommendCode::create($condition);
        }

        $agent_id = $user->agent_id;
        $info = null;
        $counter = 0;
        while ($info === null && $counter < 5) {
            $indAgent = Agent::find($agent_id);
            if ($indAgent) {
                if ($indAgent->is_independent > 0) {
                    $info = $indAgent->info ?? (AgentInfo::find(1));
                } else {
                    $agent_id = $indAgent->parent_id;
                }
            } else {
                $info = AgentInfo::find(1);
            }
            $counter++;
        }

        $web_host = $info->web_domain;
        $mobile_host = $info->mobile_domain;


        $code = $recommendCodeInstance->rec_code;
        //http://www-gubao.591wmj.com/register?code=006358
        $data = [
            'code' => $code,
            'web_host' => "http://{$web_host}/register?code={$code}",
            'mobile_host' => "http://{$mobile_host}/register?code={$code}",
        ];


        return self::jsonReturn($data);
    }

}