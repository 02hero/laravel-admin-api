<?php

namespace App\Http\Controllers\Api\Report;

use App\Http\Controllers\Controller;
use App\Http\Model\Employee;
use App\Http\Model\UEmpFullAchievementReport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class EmployeeFullReportController 员工总报表
 * @package App\Http\Controllers\Api/Report
 */
class EmployeeFullReportController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:api");
    }

    /**
     * 代理商员工表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request)
    {
        $employeeIds = Auth::user()->getAllMyEmployeeIdArrayAttribute();

        $query = UEmpFullAchievementReport::orderByDesc('occur_time')->whereIn('emp_id', $employeeIds)->with('agent', 'employee');

        $emp_id = $request->input('emp_id');
        if ($emp_id) {
            $query->where('emp_id', 'like', "%$emp_id%");
        }
        $cellphone = $request->input('cellphone');
        if ($cellphone) {
            $empIdssss = Employee::where('phone', 'like', "$cellphone")->pluck('id')->toArray();
            $query->where('emp_id', 'in', array_values($empIdssss));
        }
        $list = $query->paginate($request->input('size', self::PAGE_SIZE));

        return self::jsonReturn($list);
    }

}