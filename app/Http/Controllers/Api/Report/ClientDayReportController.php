<?php

namespace App\Http\Controllers\Api\Report;

use App\Http\Controllers\Controller;
use App\Http\Model\Agent;
use App\Http\Model\UCustDayStockFinancing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class ClientDayReportController 客户日报表
 * @package App\Http\Controllers\Api/Report
 */
class ClientDayReportController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:api");
    }

    /**
     * 代理商员工表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request)
    {
        $clientIds = Auth::user()->getAllMyClientIdArrayAttribute();

        $query = UCustDayStockFinancing::orderByDesc('occur_time')->whereIn('cust_id', $clientIds)->with('agent');

        $agent_number = $request->input('agent_number');
        if ($agent_number) {
            $agentdIds = Agent::where('agent_number', 'like', "%$agent_number%")->pluck('id')->toArray();
            $query->whereIn('agent_id', array_values($agentdIds));
        }
        $cust_cellphone = $request->input('cellphone');
        if ($cust_cellphone) {
            $query->where('cust_cellphone', 'like', "%$cust_cellphone%");
        }

        $list = $query->paginate($request->input('size', self::PAGE_SIZE));

        return self::jsonReturn($list);
    }

}