<?php

namespace App\Http\Controllers\Api\Report;

use App\Http\Controllers\Controller;
use App\Http\Model\Employee;
use App\Http\Model\UEmpDayAchievementReport;
use App\Http\Model\UEmpFullAchievementReport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class EmployeeDayReportController 员工日报表
 * @package App\Http\Controllers\Api\Report
 */
class EmployeeDayReportController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:api");
    }

    public function list(Request $request)
    {
        $employeeIds = Auth::user()->getAllMyEmployeeIdArrayAttribute();

        $query = UEmpDayAchievementReport::orderByDesc('occur_time')->whereIn('emp_id', $employeeIds)->with('full');

        $emp_id = $request->input('emp_id');
        if ($emp_id) {
            $query->where('emp_id', 'like', "%$emp_id%");
        }
        $cellphone = $request->input('cellphone');
        if ($cellphone) {
            $empIdssss = Employee::where('phone', 'like', "$cellphone")->pluck('id')->toArray();
            $query->where('emp_id', 'in', array_values($empIdssss));
        }

        $list = $query->paginate($request->input('size', self::PAGE_SIZE));

        return self::jsonReturn($list);
    }


    public function totalList(Request $request)
    {
        $employeeIds = Auth::user()->getAllMyEmployeeIdArrayAttribute();

        //计算时间段总金额
        $querySum = UEmpDayAchievementReport::whereIn('emp_id', $employeeIds);
        $queryTotalSum = UEmpFullAchievementReport::whereIn('emp_id', $employeeIds);

        $query = UEmpDayAchievementReport::orderByDesc('emp_id')->whereIn('emp_id', $employeeIds)->with('employee', 'full');
        $emp_id = $request->input('emp_id');
        if ($emp_id) {
            $query->where('emp_id', 'like', "%$emp_id%");
            $querySum->where('emp_id', 'like', "%$emp_id%");
            $queryTotalSum->where('emp_id', 'like', "%$emp_id%");

        }
        $cellphone = $request->input('cellphone');
        if ($cellphone) {
            $empIdssss = Employee::where('phone', 'like', "$cellphone")->pluck('id')->toArray();
            $query->where('emp_id', 'in', array_values($empIdssss));
            $querySum->where('emp_id', 'in', array_values($empIdssss));
            $queryTotalSum->where('emp_id', 'in', array_values($empIdssss));

        }

        $fromDate = $request->fromDate;
        if (strlen($fromDate) > 9) {
            $fromDate = Carbon::parse($fromDate)->toDateString();
            $query->whereDate('occur_time', '>=', $fromDate);
            $querySum->whereDate('occur_time', '>=', $fromDate);
            $queryTotalSum->whereDate('occur_time', '>=', $fromDate);

        }
        $toDate = $request->toDate;
        if (strlen($toDate) > 9) {
            $toDate = Carbon::parse($toDate)->toDateString();
            $query->whereDate('occur_time', '<=', $toDate);
            $querySum->whereDate('occur_time', '<=', $toDate);
            $queryTotalSum->whereDate('occur_time', '<=', $toDate);
        }

        $selectRaw = "`emp_id`,sum(`cust_day_capital_amount`) AS `cust_day_capital_amount`,sum(`cust_day_incharge`) AS `cust_day_incharge`,sum(`cust_day_cash`) AS `cust_day_cash`,sum(`cust_day_stock_finance`) AS `cust_day_stock_finance`,sum(`cust_day_makedeal`) AS `cust_day_makedeal`,sum(`cust_total_interests`) AS `cust_total_interests`,sum(`cust_total_fee`) AS `cust_total_fee`,sum(`cust_total_commision`) AS `cust_total_commision`,sum(`cust_total_adv_cost`) AS `cust_total_adv_cost`,sum(`total_interests_percentage`) AS `total_interests_percentage`,sum(`total_commision_percentage`) AS `total_commision_percentage`";
        $list = $query->groupBy('emp_id')->with('employee')->selectRaw($selectRaw)->paginate($request->input('size', self::PAGE_SIZE));


        //计算累计
        $sumData[0] = $querySum->sum('cust_day_capital_amount');//可用余额
        $sumData[1] = $querySum->sum('cust_day_stock_finance');//新增
        $sumData[2] = $queryTotalSum->sum('cust_total_stock_finance');//累计配资  员工累计报表的累计
        $sumData[3] = $querySum->sum('cust_day_makedeal');//交易金额
        $sumData[4] = $querySum->sum('total_interests_percentage');//递延费 利息
        $sumData[5] = $querySum->sum('total_commision_percentage');//服务费 手续费

        $data = $list->toArray();
        $data['sum'] = $sumData;
        return self::jsonReturn($data);
    }

}