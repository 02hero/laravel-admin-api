<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Model\ContentHelp;
use App\Http\Model\ContentHelpType;
use App\Http\Model\ContentProtocol;
use App\Http\Model\Role;
use Illuminate\Http\Request;

class ContentHelpTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:api")->except(['search']);
    }

    /**
     * 帮助类型列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function helpTypes(Request $request)
    {
        $user = $request->user();
        $per_page = $request->input('size', self::PAGE_SIZE);
        $keyword = $request->input('keyword');
        $help_id = $request->input('help_id');
        $isSelect = $request->input('is_select');
        $query = ContentHelpType::select("*");
        if ($keyword) {
            $query->orWhere('name', 'like', "%$keyword%")
                ->orWhere('id', 'like', "%$keyword%")
                ->orWhere("agent_id", "like", "%$keyword%");
        }

        $agent_id = 0;
        if ($help_id) {
            $help = ContentHelp::where('id', $help_id)->first();
            if ($help) $agent_id = $help->agent_id;
        }

        //代理商
        if ($user->role_id != Role::ROLE_ADMIN_SYSTEM || $agent_id || $isSelect) {
            $query->where("agent_id", $agent_id ? : $user->agent_id);
        }

        $data = $query->paginate($per_page);
        return self::jsonReturn($data);
    }

    /**
     * 帮助类型详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHelpTypeInfo(Request $request)
    {
        $user = \Auth::user();
        $query = ContentHelpType::where('id', $request->get("id"));
        //代理商
        if ($user->role_id != Role::ROLE_ADMIN_SYSTEM) {
            $query->where("agent_id", $user->agent_id);
        }
        $ret = $query->first();
        return  self::jsonReturn($ret);
    }

    /**
     * 编辑帮助类型
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function helpTypeEdit(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|min:1,20',
        ], [
            "name.required" => "类别名称不能为空",
            "name.min" => "类别名称应为1-20个字符",
        ]);

        if ($validator->fails()) {
            return parent::jsonReturn([], parent::CODE_FAIL, $validator->errors()->first());
        }

        $user = \Auth::user();
        $data = $request->only("name", "sort");
        foreach ($data as $k=>$v) {
            if ($v === null) $data[$k] = "";
        }

        $id = $request->get("id");
        if ($id) {
            $helpTypeQuery = ContentHelpType::where("id", $id);
            if ($user->role_id != Role::ROLE_ADMIN_SYSTEM) {
                $helpTypeQuery->where("agent_id", $user->agent_id);
            }
            $helpType = $helpTypeQuery->first();
            if (!$helpType) return self::jsonReturn([], self::CODE_FAIL, "信息错误");

            $ret = $helpType->update($data);
        } else {
            $data = array_merge($data, [
                "agent_id"=>$user->agent_id,
            ] );

            $ret = ContentHelpType::create($data);
        }

        return $ret ? self::jsonReturn([], self::CODE_SUCCESS, "操作成功") :
            self::jsonReturn([], self::CODE_FAIL, "操作失败");
    }

    /**
     * 删除帮助类型
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function helpTypeDelete(Request $request)
    {
        $user = \Auth::user();
        $query = ContentHelpType::where('id', $request->get("id"));
        //代理商
        if ($user->role_id != Role::ROLE_ADMIN_SYSTEM) {
            $query->where("agent_id", $user->agent_id);
        }
        $helpType = $query->first();
        if (!$helpType) return self::jsonReturn([], self::CODE_FAIL, "该协议不存在");

        $ret = $helpType->delete();
        return self::jsonReturn([]);
    }

}