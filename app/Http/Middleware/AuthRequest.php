<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthRequest
{
    /**
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user->is_forbid) return Controller::jsonReturn([], Controller::CODE_FAIL, '对不起,你没有权限!');
        switch ($user->role_id) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            case 11:
                break;
            case 12:
                break;
            case 13:
                break;
            case 14:
                break;
            case 15:
                break;
            case 16:
                break;
            default:
                //未知角色
                return Controller::jsonReturn([], Controller::CODE_FAIL, '对不起,你没有权限!');
        }
        return $next($request);
    }
}
